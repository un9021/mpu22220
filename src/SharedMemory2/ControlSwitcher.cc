// -----------------------------------------------------------------------------
#include "ControlSwitcher.h"
// ------------------------------------------------------------------------------------------
using namespace std;
using namespace uniset;
// ------------------------------------------------------------------------------------------
ControlSwitcher::ControlSwitcher(uniset::ObjectId id, xmlNode* confnode ):
	ControlSwitcher_SK(id, confnode)
{
	cout << "ControlSwitcher create" << endl;
}

ControlSwitcher::~ControlSwitcher()
{
}
// -----------------------------------------------------------------------------
void ControlSwitcher::step()
{
        calculateTargets();
        out_w_MPU1_Vsetting_AC = Vsetting;
        out_w_MPU1_SettingMode_AC = settingMode;
        out_w_MPU1_Psetting1_AC = P1setting;
        out_w_MPU1_Psetting2_AC = P2setting;

        out_w_rotate1=in_wi_rotate1;
	out_w_rotate2=in_wi_rotate2;
	out_wo_rotate1=in_rotate1;
	out_wo_rotate2=in_rotate2;
}
// -----------------------------------------------------------------------------
void ControlSwitcher::calculateTargets()
{
//    Датчики оканчивающиеся без цифр пишутся в ПЧ
//    Датчики оканчивающиеся на 2 берутся из ГУИ ПЧ
//    Датчики оканчивающиеся на 3 берутся с СВУ
//    Датчики оканчивающиеся на 4 пишутся в соседнем ПЧ
//    Датчики оканчивающиеся на 5 пишутся на текущем ПЧ и по модбасу передаются на 4 для соседнего ПЧ
    // Переключение задания, для разных режимов (ведущий/ведомый,местное/дистанционное)
    if (in_Master) {
        calculateTargetInMasterMode();
    }
    else {
        calculateTargetInSlaveMode();
    }
    // Управление системой охлаждения ПЧ
    if( in_RemoteControl_S ) {
        out_w_MPU1_BVO_On_C = in_w_MPU1_BVO_On_C3;
    }

    // Если в режиме Лед
    if (LedMode_s) {
        // Ограничиваем мощность до значений указанных в настройках процесса
        if ( abs(out_w_MPU1_Psetting1_AC5) > ledModeLimit ) {
            // Проверяем знак и ограничиваем
            if ( out_w_MPU1_Psetting1_AC5 > 0) {
                out_w_MPU1_Psetting1_AC5 = ledModeLimit;
            }
            else {
                out_w_MPU1_Psetting1_AC5 = - ledModeLimit;
            }
        }
    }

}
// -----------------------------------------------------------------------------
void ControlSwitcher::calculateTargetInMasterMode()
{
    if ( in_Master) //если этот ПЧ мастер - пишем в ведомый
    {
        //по-умолчанию выставляем с местного поста
        settingMode = in_w_MPU1_SettingMode_AC2;
        Vsetting = in_w_MPU1_Vsetting_AC2;
        P1setting = in_w_MPU1_Psetting1_AC2;
        P2setting = in_w_MPU1_Psetting2_AC2;
        //set local master
        mode = Mode::localMaster;

        if( in_RemoteControl_S ) //если управление с СВУ
        {
            settingMode = in_w_MPU1_SettingMode_AC3;
            Vsetting = in_w_MPU1_Vsetting_AC3;
            P1setting = in_w_MPU1_Psetting1_AC3;
            P2setting = in_w_MPU1_Psetting2_AC3;

            //set remote master
            mode = Mode::remoteMaster;
        }

        //пишем задание для второго ПЧ
        if ( in_RemoteControl_S) {
            out_w_MPU1_SettingMode_AC5 = settingMode;
            out_w_MPU1_Vsetting_AC5 = Vsetting;
            //грязный хак, пока не сделал передачу статуса, что мастер управляется от СВУ
            out_w_MPU1_Psetting1_AC5 = otherFCInRemoteControl;
            out_w_MPU1_Psetting2_AC5 = otherFCInRemoteControl;
        }
        else {
            out_w_MPU1_SettingMode_AC5 = settingMode;
            out_w_MPU1_Vsetting_AC5 = Vsetting;
            out_w_MPU1_Psetting1_AC5 = P1setting;
            out_w_MPU1_Psetting2_AC5 = P2setting;
        }
    }
}
// -----------------------------------------------------------------------------
void ControlSwitcher::calculateTargetInSlaveMode()
{
    //если режим работы по оборотам и с ведущего и с верху - берем с ведущего (хотя это задание должно быть проигнорировано на уровше контроллера отвечающего за ШИМ)
    if (in_w_MPU1_SettingMode_AC4 == setRPM && in_w_MPU1_SettingMode_AC3 == setRPM) {
        Vsetting = in_w_MPU1_Vsetting_AC4;
        settingMode = in_w_MPU1_SettingMode_AC4;
        P1setting = in_w_MPU1_Psetting1_AC4;
        P2setting = in_w_MPU1_Psetting2_AC4;

        mode = Mode::slaveFrMaster;
    }
    //какое-то рассогласование в управлении - берем с СВУ
    else if (in_w_MPU1_SettingMode_AC4 == setRPM && in_w_MPU1_SettingMode_AC3 == setPower) {
        Vsetting = in_w_MPU1_Vsetting_AC4;
        settingMode = in_w_MPU1_SettingMode_AC4;
        P1setting = in_w_MPU1_Psetting1_AC4;
        P2setting = in_w_MPU1_Psetting2_AC4;

        mode = Mode::slaveFrMasterWithProblems;
    }
    //скорее всего ведущий просто разобрался, берем задание с СВУ
    else if (in_w_MPU1_SettingMode_AC4 == setRPM && in_w_MPU1_SettingMode_AC3 == setPower && in_w_MPU1_Vsetting_AC4==0) {
        settingMode = in_w_MPU1_SettingMode_AC3;
        Vsetting = in_w_MPU1_Vsetting_AC3;
        P1setting = in_w_MPU1_Psetting1_AC3;
        P2setting = in_w_MPU1_Psetting2_AC3;

        mode = Mode::slaveFrSVUMasterDown;
    }
    //если оба в режиме задания по мощности - берем мощность с верхнего уровня
    else if (in_w_MPU1_SettingMode_AC4 == setPower && in_w_MPU1_SettingMode_AC3 == setPower){
        settingMode = in_w_MPU1_SettingMode_AC4;
        Vsetting = in_w_MPU1_Vsetting_AC3;
        P1setting = in_w_MPU1_Psetting1_AC3;
        P2setting = in_w_MPU1_Psetting2_AC3;

        mode = Mode::slavePowerFrSVU;
    }
    //если ведущий присылает режим задания по мощности
    else {
        //грязный хак, пока не сделал передачу статуса, что мастер в режиме удаленного управления
        //значит берем задание от СВУ
        if ( (in_w_MPU1_Psetting1_AC4 == in_w_MPU1_Psetting2_AC4) && (in_w_MPU1_Psetting2_AC4 == otherFCInRemoteControl) ) {
            settingMode = in_w_MPU1_SettingMode_AC4;
            Vsetting = in_w_MPU1_Vsetting_AC3;
            P1setting = in_w_MPU1_Psetting1_AC3;
            P2setting = in_w_MPU1_Psetting2_AC3;

            mode = Mode::slavePowerFrSVU;
        }
        else {
            //если он в режиме локального управления
            //сюда не должны заходить.. так как посылаемое задание не верно
            settingMode = in_w_MPU1_SettingMode_AC4;
            Vsetting = in_w_MPU1_Vsetting_AC4;
            P1setting = in_w_MPU1_Psetting1_AC4;
            P2setting = in_w_MPU1_Psetting2_AC4;

            mode = Mode::slavePowerFrMaster;
        }
    }

}
// -----------------------------------------------------------------------------
void ControlSwitcher::sensorInfo( const uniset::SensorMessage* sm )
{
}
// -----------------------------------------------------------------------------
std::string ControlSwitcher::getMonitInfo() const
{
    ostringstream s;
    s << "mode = " << mode
    << endl;

    return std::move(s.str());
}


