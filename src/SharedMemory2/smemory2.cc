#include <string>
#include <sys/wait.h>
#include <errno.h>
#include <Debug.h>
#include <UniSetActivator.h>
#include <ThreadCreator.h>
#include <CanExceptions.h>
#include <modbus/ModbusTypes.h>
#include <extensions/MBSlave.h>
#include <extensions/MBTCPMultiMaster.h>
#include <extensions/Extensions.h>
#include <ExchangeServer.h>
#include "MPUConfiguration.h"
#include "MPUSharedMemory.h"
#include "ControlSwitcher.h"
#include "CommonLamp.h"
#include <UHelpers.h>
// --------------------------------------------------------------------------
using namespace uniset;
using namespace std;
using namespace MPU;
// --------------------------------------------------------------------------
static void help_print( int argc, const char* argv[] );
static xmlNode* o_init( const std::string& cname, uniset::ObjectId& id, const std::string& secname );
// --------------------------------------------------------------------------
int main( int argc, char* argv[] )
{
	try
	{
		uniset::uniset_init(argc, argv, "configure.xml");
		auto conf = uniset::uniset_init(argc, argv, "configure.xml");
		string logfilename = conf->getArgParam("--logfile", "smemory2.log");
		string logname( conf->getLogDir() + logfilename );
		MPU::dlog.logFile( logname.c_str() );
		conf->initLogStream(dlog, "dlog");
		auto act = UniSetActivator::Instance();
		// ------------ SharedMemory ----------------
		auto shm = MPUSharedMemory::init_mpusmemory(argc, argv);
		
		if ( !shm )
			return 1;
			
		act->add(shm);
		// ------------- FQC Exchange --------------
		auto es = ExchangeServer::init_exchange(argc, const_cast<char**>(argv), shm);
		
		if ( !es )
			return 1;
			
		auto es_thr = new ThreadCreator<ExchangeServer>(es.get(), &ExchangeServer::execute);
		
		if ( !es_thr )
			return 1;
			
		std::shared_ptr<ControlSwitcher> cs;
		std::shared_ptr<CommonLamp> cl;
		
		// ------------- CommonLamp --------------
		try
		{
			cl = make_object<CommonLamp>("CommonLamp", "CommonLamp");
			act->add(cl);
		}
		catch ( std::exception& ex )
		{
			dlog[Debug::CRIT]  << "(smemory2): create CommonLamp: " << ex.what() << endl;
			return 1;
		}
		catch ( ... )
		{
			dlog[Debug::CRIT] << "(smemory2): create CommonLamp: catch ..." << endl;
			return 1;
		}
		
		// ------------- ControlSwitcher --------------
		try
		{
			cs = make_object<ControlSwitcher>("ControlSwitcher", "ControlSwitcher");
			act->add(cs);
		}
		catch ( std::exception& ex )
		{
			dlog[Debug::CRIT]  << "(smemory2): create ControlSwticher: " << ex.what() << endl;
			return 1;
		}
		catch ( ... )
		{
			dlog[Debug::CRIT] << "(smemory2): create ControlSwitcher: catch ..." << endl;
			return 1;
		}
		
		// ------------- ModbusTCP Master (GATE) --------------
		try
		{
			bool skip_mbm   = findArgParam("--skip-mbm", argc, argv) != -1;
			
			if ( !skip_mbm )
			{
				std::shared_ptr<MBTCPMultiMaster> mbm = MBTCPMultiMaster::init_mbmaster(argc, argv, shm->getId(), shm, "mbtcp");
				
				if ( !mbm )
					return 1;
					
				act->add(mbm);
			}
		}
		catch ( std::exception& ex )
		{
			dlog[Debug::CRIT] << "(smemory2): create MBMasterGATE: " << ex.what() << endl;
		}
		catch ( ... )
		{
			dlog[Debug::CRIT] << "(smemory2): create MBMasterGATE: catch ..." << endl;
		}
		
		// ------------- ModbusTCP Slave (GUI) --------------
		try
		{
			bool skip_mb    = findArgParam("--skip-mbs1", argc, argv) != -1;
			
			if ( !skip_mb )
			{
				auto mbs = MBSlave::init_mbslave(argc, argv, shm->getId(), shm, "mbs1");
				
				if ( !mbs )
					return 1;
					
				act->add(mbs);
			}
		}
		catch ( std::exception& ex )
		{
			dlog[Debug::CRIT] << "(smemory2): create MBSlave1: " << ex.what() << endl;
		}
		catch ( ... )
		{
			dlog[Debug::CRIT] << "(smemory2): create MBSlave1: catch ..." << endl;
		}
		
		// ------------- ModbusTCP Slave (SVU) --------------
		try
		{
			bool skip_mb2   = findArgParam("--skip-mbs2", argc, argv) != -1;
			
			if ( !skip_mb2 )
			{
				auto mbs2 = MBSlave::init_mbslave(argc, argv, shm->getId(), shm, "mbs2");
				
				if ( !mbs2 )
					return 1;
					
				act->add(mbs2);
			}
		}
		catch ( std::exception& ex )
		{
			dlog[Debug::CRIT] << "(smemory2): create MBSlave2: " << ex.what() << endl;
		}
		catch ( ... )
		{
			dlog[Debug::CRIT] << "(smemory2): create MBSlave2: catch ..." << endl;
		}
		
		// ------------- ModbusTCP Slave (SVU) --------------
		try
		{
			bool skip_svu   = findArgParam("--skip-svu1", argc, argv) != -1;
			
			if ( !skip_svu )
			{
				auto svu1 = MBSlave::init_mbslave(argc, argv, shm->getId(), shm, "svu1");
				
				if ( !svu1 )
					return 1;
					
				act->add(svu1);
			}
		}
		catch ( std::exception& ex )
		{
			dlog[Debug::CRIT] << "(smemory2): create MBSlaveSVU1: " << ex.what() << endl;
		}
		catch ( ... )
		{
			dlog[Debug::CRIT] << "(smemory2): create MBSlaveSVU1: catch ..." << endl;
		}
		
		// ------------- ModbusTCP Slave (SVU) --------------
		try
		{
			bool skip_svu2  = findArgParam("--skip-svu2", argc, argv) != -1;
			
			if ( !skip_svu2 )
			{
				auto svu2 = MBSlave::init_mbslave(argc, argv, shm->getId(), shm, "svu2");
				
				if ( !svu2 )
					return 1;
					
				act->add(svu2);
			}
		}
		catch ( std::exception& ex )
		{
			dlog[Debug::CRIT] << "(smemory2): create MBSlaveSVU2: " << ex.what() << endl;
		}
		catch ( ... )
		{
			dlog[Debug::CRIT] << "(smemory2): create MBSlaveSVU2: catch ..." << endl;
		}
		
		// ----------------------------------------
		if ( es_thr )
			es_thr->start();
			
		// ----------------------------------------
		SystemMessage sm(SystemMessage::StartUp);
		act->broadcast( sm.transport_msg() );
		act->run(false);
		return 0;
	}
	catch (CanOpen::CanException& ex)
	{
		dlog[Debug::CRIT] << "(smemory2): " << ex << endl;
	}
	catch ( ModbusRTU::mbException& ex )
	{
		dlog[Debug::CRIT] << "(smemory2): " << ex << endl;
	}
	catch (Exception& ex)
	{
		dlog[Debug::CRIT] << "(smemory2): " << ex << endl;
	}
	catch ( CORBA::SystemException& ex )
	{
		dlog[Debug::CRIT] << "(smemory2): " << ex.NP_minorString() << endl;
	}
	catch ( std::exception& e )
	{
		dlog[Debug::CRIT] << "(smemory2): " << e.what() << endl;
	}
	catch (...)
	{
		dlog[Debug::CRIT] << "(smemory2): catch(...)" << endl;
	}
	
	MPU::on_sigchild(SIGTERM);
	return 1;
}
// --------------------------------------------------------------------------
void help_print( int argc, const char* argv[] )
{
	cout << "--logfile fname     - save log for fname. Default: smemory2.log" << endl;
	cout << "--skip-mbs1      - Don`t run ModbusTCP slave for 1 channel for GUI" << endl;
	cout << "--skip-mbs1      - Don`t run ModbusTCP slave for 2 channel for GUI" << endl;
	cout << "--skip-svu1      - Don`t run ModbusTCP slave for 1 channel for SVU" << endl;
	cout << "--skip-svu2      - Don`t run ModbusTCP slave for 2 channel for SVU" << endl;
	cout << "--skip-mbm      - Don`t run ModbusTCP master for friend FQC" << endl;
	cout << "--skip-can			- пропустить запуск CAN" << endl;
	cout << "--skip-rs		- пропустить запуск RS" << endl;
	cout << "\n   ###### SM options ###### \n" << endl;
	MPUSharedMemory::help_print(argc, argv);
	cout << "\n   ###### MBS options ###### \n" << endl;
	MBSlave::help_print(argc, argv);
	cout << "\n   ###### ExchangeServer options ###### \n" << endl;
	ExchangeServer::help_print(argc, argv);
}
// -----------------------------------------------------------------------------
