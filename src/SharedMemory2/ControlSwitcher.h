// ------------------------------------------------------------------------------------------
#ifndef ControlSwitcher_H_
#define ControlSwitcher_H_
// ------------------------------------------------------------------------------------------
#include "ControlSwitcher_SK.h"
#include "DelayTimer.h"
// ------------------------------------------------------------------------------------------
/**
 * @brief The ControlSwitcher собирает управляющие сигналы с Океана, с графстанции МПУ, с графстанции МПУ "парного" ПЧ
 * В контроллер управляющий ШИМ(НИО12) В зависимости от положения переключателей (МЕСТНОЕ/ДИСТАНЦИОННОЕ) и (ВЕДУЩИЙ/ВЕДОМЫЙ)
 * отправляется задание
 */
class ControlSwitcher:
	public ControlSwitcher_SK
{
	public:
		ControlSwitcher( uniset::ObjectId id, xmlNode* cnode );
		virtual ~ControlSwitcher();
		
		
	protected:
		virtual void step() override;
		virtual void sensorInfo( const uniset::SensorMessage* sm ) override;
                virtual std::string getMonitInfo() const override;

	private:
                /** Расчитываем откуда брать задание (местный пост/СВУ/ведущий ПЧ)
                 * @brief calculateSettings
                 */
                void calculateTargets();
                void calculateTargetInMasterMode();
                void calculateTargetInSlaveMode();

                long Vsetting; ///< Расчитанное задание по скорости
                long settingMode; ///< Расчитанный режим задания (обороты/мощность)
                long P1setting; ///< Расчитанное задание по мощности для ПЧ1
                long P2setting; ///< Расчитанное задание по мощности для ПЧ2
                enum FCManageMode{
                    unknown=0,
                    setRPM=1,
                    setPower=2
                };
                enum Code {
                    otherFCInRemoteControl=77
                };
                int mode={-1};
                enum Mode {
                    localMaster=0,                  //set local master
                    remoteMaster=1,                 //set remote master
                    slaveFrMaster=2,                //set slave from master
                    slaveFrMasterWithProblems=3,    //set slave from master with problems
                    slaveFrSVUMasterDown=4,         //set slave from svu, master down
                    slavePowerFrSVU=5,              //set slave power svu mode
                    slavePowerFrMaster=6            //set slave power from master
                };
};
// ------------------------------------------------------------------------------------------
#endif // ControlSwitcher_H_
