// ------------------------------------------------------------------------------------------
#ifndef Imitator_H_
#define Imitator_H_
// ------------------------------------------------------------------------------------------
#include "Imitator_SK.h"
#include "DelayTimer.h"
// ------------------------------------------------------------------------------------------

class Imitator:
	public Imitator_SK
{
	public:
		Imitator( uniset::ObjectId id, xmlNode* cnode );
		virtual ~Imitator();
		
		
	protected:
		virtual void step() override;
		virtual void sensorInfo( const uniset::SensorMessage* sm ) override;
	private:
};
// ------------------------------------------------------------------------------------------
#endif // Imitator_H_
