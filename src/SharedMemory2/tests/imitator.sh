#!/bin/sh
echo "start imitator"
while true; do
	clear
	setmode=`ctl-admin-mpu22220.sh --getValue 5216 | grep value | sed s/[^0-9]//g`
	ctl-admin-mpu22220.sh -x 216=$setmode
	if [ "$setmode" -eq 1 ]
	then
		echo "Режим по оборотам"
		vset=`ctl-admin-mpu22220.sh --getValue 5217 | grep value | sed s/[^0-9]//g`
		ctl-admin-mpu22220.sh -x 217=$vset
		ctl-admin-mpu22220.sh -x 214=$vset
		ctl-admin-mpu22220.sh -x 231=$(($vset*10))
	fi
	if [ "$setmode" -eq 2 ]
	then
		echo "Режим по мощности"
		pset1=`ctl-admin-mpu22220.sh --getValue 5218 | grep value | sed s/[^0-9]//g`
		ctl-admin-mpu22220.sh -x 218=$pset1
		pset2=`ctl-admin-mpu22220.sh --getValue 5219 | grep value | sed s/[^0-9]//g`
		ctl-admin-mpu22220.sh -x 219=$pset2
		ctl-admin-mpu22220.sh -x 231=$pset1
		ctl-admin-mpu22220.sh -x 214=$(($pset1/10))
	fi


	master=`ctl-admin-mpu22220.sh --getValue 90000 | grep value | sed s/[^0-9]//g`
	ctl-admin-mpu22220.sh -x 233=$master
	remote=`ctl-admin-mpu22220.sh --getValue 90001 | grep value | sed s/[^0-9]//g`
	ctl-admin-mpu22220.sh -x 238=$remote


done;
