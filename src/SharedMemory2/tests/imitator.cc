#include <string>
#include <sys/wait.h>
#include <errno.h>
#include <Debug.h>
#include <UniSetActivator.h>
#include <ThreadCreator.h>
#include <CanExceptions.h>
#include <modbus/ModbusTypes.h>
#include <extensions/MBSlave.h>
#include <extensions/MBTCPMultiMaster.h>
#include <extensions/Extensions.h>
#include <ExchangeServer.h>
#include "MPUConfiguration.h"
#include "MPUSharedMemory.h"
#include "Imitator.h"
#include <UHelpers.h>
// --------------------------------------------------------------------------
using namespace uniset;
using namespace std;
using namespace MPU;
// --------------------------------------------------------------------------
static void help_print( int argc, const char* argv[] );
static xmlNode* o_init( const std::string& cname, uniset::ObjectId& id, const std::string& secname );
// --------------------------------------------------------------------------
int main( int argc, char* argv[] )
{
	try
	{
		uniset::uniset_init(argc, argv, "configure.xml");
		auto conf = uniset::uniset_init(argc, argv, "configure.xml");
		string logfilename = conf->getArgParam("--logfile", "smemory2.log");
		string logname( conf->getLogDir() + logfilename );
		MPU::dlog.logFile( logname.c_str() );
		conf->initLogStream(dlog, "dlog");
		auto act = UniSetActivator::Instance();
		// ------------ SharedMemory ----------------
			auto shm = MPUSharedMemory::init_mpusmemory(argc, argv);
	
	if ( !shm )
	    return 1;
	    
	act->add(shm);
	std::shared_ptr<Imitator> im;	
		// -------------Imitator --------------
	try
	{
	    im = make_object<Imitator>("Imitator", "Imitator");
	    act->add(im);
	}
	catch ( std::exception& ex )
	{
	    dlog[Debug::CRIT]  << "(smemory2): create ControlSwticher: " << ex.what() << endl;
	    return 1;
	}
	catch ( ... )
	{
	    dlog[Debug::CRIT] << "(smemory2): create Imitator: catch ..." << endl;
	    return 1;
	}

		
		
		
		SystemMessage sm(SystemMessage::StartUp);
		act->broadcast( sm.transport_msg() );
		act->run(true);
		return 0;
	}
	catch (CanOpen::CanException& ex)
	{
		dlog[Debug::CRIT] << "(smemory2): " << ex << endl;
	}
	catch ( ModbusRTU::mbException& ex )
	{
		dlog[Debug::CRIT] << "(smemory2): " << ex << endl;
	}
	catch (Exception& ex)
	{
		dlog[Debug::CRIT] << "(smemory2): " << ex << endl;
	}
	catch ( CORBA::SystemException& ex )
	{
		dlog[Debug::CRIT] << "(smemory2): " << ex.NP_minorString() << endl;
	}
	catch ( std::exception& e )
	{
		dlog[Debug::CRIT] << "(smemory2): " << e.what() << endl;
	}
	catch (...)
	{
		dlog[Debug::CRIT] << "(smemory2): catch(...)" << endl;
	}
	
	MPU::on_sigchild(SIGTERM);
	return 1;
}
// --------------------------------------------------------------------------
void help_print( int argc, const char* argv[] )
{
	cout << "--logfile fname     - save log for fname. Default: smemory2.log" << endl;
	cout << "--skip-mbs1      - Don`t run ModbusTCP slave for 1 channel for GUI" << endl;
	cout << "--skip-mbs1      - Don`t run ModbusTCP slave for 2 channel for GUI" << endl;
	cout << "--skip-svu1      - Don`t run ModbusTCP slave for 1 channel for SVU" << endl;
	cout << "--skip-svu2      - Don`t run ModbusTCP slave for 2 channel for SVU" << endl;
	cout << "--skip-mbm      - Don`t run ModbusTCP master for friend FQC" << endl;
	cout << "--skip-can			- пропустить запуск CAN" << endl;
	cout << "--skip-rs		- пропустить запуск RS" << endl;
	cout << "\n   ###### SM options ###### \n" << endl;
	MPUSharedMemory::help_print(argc, argv);
	cout << "\n   ###### MBS options ###### \n" << endl;
	MBSlave::help_print(argc, argv);
	cout << "\n   ###### ExchangeServer options ###### \n" << endl;
	ExchangeServer::help_print(argc, argv);
}
// -----------------------------------------------------------------------------
