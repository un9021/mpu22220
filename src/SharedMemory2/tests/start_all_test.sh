#!/bin/sh

START=uniset2-start.sh
${START} -f uniset2-testsuite-xmlplayer \
--confile configure.xml \
--testfile all_test.xml \
--log-show-tests \
--log-show-actions \
--log-show-comments \
--log-show-numline \
--log-show-timestamp \
--log-show-test-filename \
--log-show-test-comment \
--log-show-test-type \
--default-timeout 15000 \
--default-check-pause 200
