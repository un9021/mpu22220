// -----------------------------------------------------------------------------
#include "Imitator.h"
// ------------------------------------------------------------------------------------------
using namespace std;
using namespace uniset;
// ------------------------------------------------------------------------------------------
Imitator::Imitator(uniset::ObjectId id, xmlNode* confnode ):
	Imitator_SK(id, confnode)
{
	cout << "Imitator create" << endl;
}

Imitator::~Imitator()
{
}
// -----------------------------------------------------------------------------
void Imitator::step()
{
	out_target_rpm_confirm=in_target_rpm;
	out_target_power_confirm=in_target_power;
	out_target_mode_confirm=in_target_mode;
	out_master_confirm=in_master;
	out_remote_confirm=in_remote;
}
// -----------------------------------------------------------------------------
void Imitator::sensorInfo( const uniset::SensorMessage* sm )
{
}
