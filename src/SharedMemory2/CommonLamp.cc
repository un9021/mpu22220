// -----------------------------------------------------------------------------
#include "CommonLamp.h"
// ------------------------------------------------------------------------------------------
using namespace std;
using namespace uniset;
// ------------------------------------------------------------------------------------------
CommonLamp::CommonLamp(uniset::ObjectId id, xmlNode* confnode ):
	CommonLamp_SK(id, confnode)
{
	cout << "CommonLamp create" << endl;
}

CommonLamp::~CommonLamp()
{
}
// -----------------------------------------------------------------------------
void CommonLamp::step()
{
	(in_respond1 || in_respond2) ? out_respondresult = true : out_respondresult = false;
}
// -----------------------------------------------------------------------------
void CommonLamp::sensorInfo( const uniset::SensorMessage* sm )
{
}
