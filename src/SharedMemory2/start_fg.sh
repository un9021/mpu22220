#!/bin/sh

#ulimit -Sc 10000000
START=uniset2-start.sh

#	 --c-filter-field tsnode --c-filter-value 1 --heartbeat-node ts --lock-value-pause 10 \

${START} -f ./smemory2-mpu22220 \
	--smemory-id SharedMemory1 \
	--Control-log-add-levels any \
	--ControlSwitcher-log-add-levels any \
	--dlog-add-levels any \
	--unideb-add-levels any \
	--skip-rs 1 \
	--skip-can 1 \
	--sm-no-history 1 \
	--rs-use-485 0 \
	--rs-dev /dev/null \
	--can-dev /dev/null \
	--skip-mbm \
	--mbs1-name MBSlave1 \
	--mbs1-type TCP \
	--mbs1-inet-addr localhost \
	--mbs1-inet-port 2050 \
	--mbs1-filter-field tcpgui \
	--mbs1-filter-value MPU1 \
	--mbs1-reg-from-id 0 \
	--mbs1-force 1 \
	--mbs1-set-prop-prefix mbgui_ \
	--can--mbreg-propname mbreg \
	--rs-mbreg-propname mbreg \
	--mbtcp-name MBTCPMultiMasterGATE \
	--mbtcp-gateway-iaddr 192.168.177.11 \
	--mbtcp-gateway-port 500 \
	--mbtcp-filter-field tcpgate \
	--mbtcp-filter-value GATE \
	--mbtcp-set-prop-prefix mbgate_ \
	--mbs2-name MBSlave2 \
	--mbs2-type TCP \
	--mbs2-inet-addr localhost \
	--mbs2-inet-port 2051 \
	--mbs2-filter-field tcpgui \
	--mbs2-filter-value MPU2 \
	--mbs2-reg-from-id 0 \
	--mbs2-set-prop-prefix mbgui_ \
	--mbs2-force 1 \
	--svu1-name MBSlaveSVU1 \
	--svu1-type TCP \
	--svu1-inet-addr 192.168.177.100 \
	--svu1-inet-port 2048 \
	--svu1-filter-field tcpsvu \
	--svu1-filter-value MPU1 \
	--svu1-reg-from-id 0 \
	--svu1-force 1 \
	--svu1-set-prop-prefix mbsvu_ \
	--svu2-name MBSlaveSVU2 \
	--svu2-type TCP \
	--svu2-inet-addr localhost \
	--svu2-inet-port 2049 \
	--svu2-filter-field tcpsvu \
	--svu2-filter-value MPU2 \
	--svu2-reg-from-id 0 \
	--svu2-force 1 \
	--svu2-set-prop-prefix mbsvu_ \


	$*

#	--mbs-filter-field rs --mbs-filter-value 1 \
# --unideb-add-levels info,warn,crit
