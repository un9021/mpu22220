// ------------------------------------------------------------------------------------------
#ifndef CommonLamp_H_
#define CommonLamp_H_
// ------------------------------------------------------------------------------------------
#include "CommonLamp_SK.h"
#include "DelayTimer.h"
// ------------------------------------------------------------------------------------------

class CommonLamp:
	public CommonLamp_SK
{
	public:
		CommonLamp( uniset::ObjectId id, xmlNode* cnode );
		virtual ~CommonLamp();
		
		
	protected:
		virtual void step() override;
		virtual void sensorInfo( const uniset::SensorMessage* sm ) override;
	private:
};
// ------------------------------------------------------------------------------------------
#endif // CommonLamp_H_
