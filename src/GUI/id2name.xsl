<?xml version="1.0" encoding="UTF-8"?>
    <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:output method="xml" indent="yes"/>
    
<xsl:template match="@* | node()">
    <xsl:copy>
        <xsl:apply-templates select="@* | node()" />
    </xsl:copy>
</xsl:template>

<xsl:template match="object/property">
	<xsl:variable name="count">
		<xsl:value-of select="count(property[@name='name']) " />
	</xsl:variable>
	<xsl:if test="$count=0">
		<xsl:if test="../@class!='GtkCellRendererText'">
		<xsl:if test="../@class!='GtkTreeViewColumn'">
		<xsl:if test="../@class!='GtkAdjustment'">
			<xsl:if test="position()=4"><property name="name"><xsl:value-of select="../@id"/></property>
		</xsl:if>
		</xsl:if>
		</xsl:if>
		</xsl:if>
	</xsl:if>
	<!-- <xsl:element name='pv'><xsl:copy-of select="position()"/></xsl:element> -->
	<xsl:copy-of select="."/>
</xsl:template>

</xsl:stylesheet>