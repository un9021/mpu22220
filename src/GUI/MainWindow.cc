#include "MainWindow.h"
// -------------------------------------------------------------------------
using namespace MPU;
using namespace uniset;
// -------------------------------------------------------------------------
Glib::RefPtr<MainWindow> MainWindow::inst;
// -------------------------------------------------------------------------
Glib::RefPtr<MainWindow> MainWindow::Instance()
{
	return inst;
}
// -------------------------------------------------------------------------
Glib::RefPtr<MainWindow> MainWindow::init_gui( uniset::ObjectId id, int argc, char* argv[], std::shared_ptr<IONotifyController>& nc )
{
	Glib::RefPtr<Gtk::Builder> refXml = GUIConf::GladeXmlInstance();
	GUIXml_ref guixml = GUIConf::GUIXmlInstance();

	if( !inst )
		inst = Glib::RefPtr<MainWindow>( new MainWindow(id, nc) );

	return inst;
}
// -------------------------------------------------------------------------
extern void setNumLock( bool state, bool no_emit = false );
// -------------------------------------------------------------------------
Gtk::Window* MainWindow::getWin()
{
	return pMainWindow;
}
// -------------------------------------------------------------------------
MainWindow::MainWindow( uniset::ObjectId id, std::shared_ptr<IONotifyController>& nc ):
	BaseMainWindow(id, nc, "MainWindow"),
	winClock(0),
	md(0),
	bm(0),
	ewin(0),
	sp(0),
	pMainWindow(0),
	lblTime(0),
	lblDate(0),
	btnMainDate(0),
	btnJournal(0),
	btnJournalall(0),
	btnJournalnow(0),
	btnState(0),
	btnControl(0),
	btnSetting(0),
	btnCooling(0),
	btnCoolingBack(0),
	btnShow(0),
	btnExit(0),
	notebook(0),
	notebookbm(0),
	notebookjrn(0),
	journal(0),
	tstate(0),
	boxstate(0),
	fqcmenu(0),
	psplash(0),
	btnJournalrem(0),
	tm_checknetwork_msec(0)
{
	Glib::RefPtr<Gtk::Builder> refXml = GUIConf::GladeXmlInstance();
	static struct timeval tbeg;
	getTime(tbeg);
	myname = "MainWindow";
	pMainWindow = 0;
	GETWIDGET(pMainWindow, "MainWindow");
	pMainWindow->hide();
	pMainWindow->property_user_data() = pMainWindow;
	pFullScreenSheme = 0;
	GETWIDGET(pFullScreenSheme, "FullScreenSheme");
	pFullScreenSheme->hide();
	pFullScreenSheme->set_decorated(false);
	GUIXml_ref guixml = GUIConf::GUIXmlInstance();
	sp = new ProcessProgress();
	sp->write(5);
	xmlNode* cnode = guixml->findNode(guixml->getFirstNode(), "MainWindow");
	auto conf = uniset_conf();

	if( cnode == NULL )
		throw SystemError("Not found xml-confnode for MainWindow");

	onflashNode = guixml->findNode(cnode, "OnFlashList");

	if( dlog.debugging(Debug::INFO) )
		dlog[Debug::INFO] << myname << "(init): onFlashList " << (onflashNode ? "yes" : "no") << endl;

	UniXML_iterator it(cnode);
	tm_checknetwork_msec = guixml->getPIntProp(cnode, "tm_checknetwork_msec", 60000);
	// --------------------------------------------------------------
	lblTime = 0;
	GETWIDGET(lblTime, "lblTime");
	Pango::FontDescription fn("Helvetica Bold 14");
	lblTime->modify_font(fn);
	lblDate = 0;
	GETWIDGET(lblDate, "lblDate");
	Pango::FontDescription fn1("Helvetica Bold 14");
	lblDate->modify_font(fn1);
	// -------------------------------------------------------------
	SHOW_TIME(myname, "get widgets...");
	// --------------------------------------------------------------
	// --------------------------------------------------------------
	dlgSetVal	= SetValueDialog::Instance();
	SHOW_TIME(myname, "instance dialogs...");
	// --------------------------------------------------------------
	sp->write(30);
	// --------------------------------------------------------------
	SHOW_TIME(myname, "get buttons...");
	// --------------------------------------------------------------
	ctl = Control::create("Control1",shm, "ctl");
	set = Setting::create("Setting1", "set");
	sta = new State("State1", "sta", shm, this);
	dlgPass		= PasswordDialog::Instance();
	dlgCPass    = ChangePasswordDialog::Instance();
	ewin = new EngineerWindow( "EngineerWindow" , shm);
	GETWIDGET(notebook, "notebook1");
	GETWIDGET(notebookjrn, "notebookjrn"); //Журнал
	GETWIDGET(journal, "Journal");
	notebookjrn->set_current_page(0);
	/* Настройка*/
	//Настройка даты
	Gtk::Button* btnDate = 0;
	//GETWIDGET(btnDate, "ctl_btn_set_datetime");
	//btnDate->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnDate_clicked));
	winClock = manage( new ClockWindow() );
	//Обработчик двойного нажатия по мнемосхеме
	GETWIDGET(mnemoShemeEv, "mnemoShemeEv");
	mnemoShemeEv->signal_event().connect(sigc::mem_fun(*this, &MainWindow::btnShow_clicked));
	//Обработчик нажатия на МТ
	GETWIDGET(btnMT, "btnMT");
	btnMT->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnMT_clicked));
	//mnemoShemeEv.set_events(~Gdk::ALL_EVENTS_MASK);
	//Обработчик нажатия кнопки Закрыть мнемосхему
	GETWIDGET(btnExit, "ctl_btn_Exit");
	btnExit->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnExit_clicked));
	//Обработчик открытия настройки времени в главном окне
	//GETWIDGET(btnMainDate, "main_btnTime");
	//btnMainDate->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnDate_clicked));
	//Главное меню
	//Обработчик нажатия кнопки Состояния
	GETWIDGET(btnState, "btn_main_state");
	btnState->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnState_clicked));
	btnState->set_active(1);
	//Обработчик нажатия кнопки Управление
	GETWIDGET(btnControl, "btn_main_control");
	btnControl->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnControl_clicked));
	//Обработчик нажатия кнопки Настройка
	GETWIDGET(btnSetting, "btn_main_setting");
	btnSetting->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnSetting_clicked));
	/*Нижнее меню*/
	//Обработчик нажатия кнопки Журнал
	GETWIDGET(btnJournal, "main_btnJournal");
	btnJournal->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnJournal_clicked));
	//Обработчик нажатия кнопки Журнал
	GETWIDGET(btnEnwinExit, "btnExit1");
	btnEnwinExit->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnEnwinExit_clicked));
	//Обработчик нажатия кнопки Все в Журнале
	GETWIDGET(btnJournalall, "ctl_btn_journalall");
	btnJournalall->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnJournalAll_clicked));
	//Обработчик нажатия кнопки Текущие в Журнале
	GETWIDGET(btnJournalnow, "ctl_btn_journalnow");
	btnJournalnow->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnJournalNow_clicked));
	//Обработчик нажатия кнопки Удаленный в Журнале
	GETWIDGET(btnJournalrem, "ctl_btn_journal_remote");
	btnJournalrem->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnJournalRem_clicked));
	//Обработчик нажатия кнопки охлаждение ПЧ
	GETWIDGET(btnCooling, "btnCooling");
	btnCooling->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btncooling_clicked));
	//Обработчик нажатия кнопки выхода из охлаждение ПЧ
	GETWIDGET(btnCoolingBack, "ctl_btn_back");
	btnCoolingBack->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnbackcooling_clicked));
	//обработчики U,I,P,T на большой на мнемосхеме
	GETWIDGET(btnU, "ctl_btnU");
	btnU->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnU_clicked));
	GETWIDGET(btnI, "ctl_btnI");
	btnI->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnI_clicked));
	GETWIDGET(btnP, "ctl_btnP");
	btnP->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnP_clicked));
	GETWIDGET(btnT, "ctl_btnT");
	btnT->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::btnT_clicked));
	GETWIDGET(tablebtn, "tablebtn");
	GETWIDGET(fixU, "fixU");
	GETWIDGET(fixI, "fixI");
	GETWIDGET(fixP, "fixP");
	GETWIDGET(fixT, "fixT");
	fixU->hide();
	fixI->hide();
	fixP->hide();
	fixT->hide();
	GETWIDGET(notebookbm, "notebookbm");
	GETWIDGET(boxstate, "mainframe");
	GETWIDGET(tstate, "tableState");
	GETWIDGET(fqcmenu, "fqcmenu1");
	jwin = WarningsJournal::Instance(shm);
	jwin->signal_show_alarm().connect(sigc::mem_fun(*this, &MainWindow::on_showAlarmJournal));
	jwin->show();
	awin = AlarmWindow::Instance();
	awin->show();
	//wman = WinManager::Instance();
	//wman->setMainWindow(ewin);
	wwin = WarningsWindow::Instance(shm);
	wwin->signal_list_add_item().connect(sigc::mem_fun(*this, &MainWindow::on_new_warning));
	wwin->signal_show_alarm().connect(sigc::mem_fun(*this, &MainWindow::on_showAlarmWarning));
	//wwinremote = new UserWarningsWindow("WarningsWindowRemote", shm, "", awin);
	//wwinremote->signal_list_add_item().connect(sigc::mem_fun(*this, &MainWindow::on_new_warning));
	//wwinremote->signal_show_alarm().connect(sigc::mem_fun(*this, &MainWindow::on_showAlarmWarning));
	//wwinlocal = new UserWarningsWindow("WarningsWindowLocal", shm, "", awin);
	//wwinlocal->signal_list_add_item().connect(sigc::mem_fun(*this, &MainWindow::on_new_warning));
	//wwinlocal->signal_show_alarm().connect(sigc::mem_fun(*this, &MainWindow::on_showAlarmWarning));
	flash = new FlashLed("FlashLed", shm, this, refXml);
	flash->signal_flash_state().connect(sigc::mem_fun(*this, &MainWindow::on_flash_state));
	// --------------------------------------------------------------
	sp->write(40);
	// --------------------------------------------------------------
	sp->write(60);
	// --------------------------------------------------------------
	connTime.disconnect();
	connTime = Glib::signal_timeout().connect(sigc::mem_fun(*this, &MainWindow::setTime), 1000);
	setDate(); // выставим дату текущую
	connDate.disconnect();
	connDate = Glib::signal_timeout().connect(sigc::mem_fun(*this, &MainWindow::setDate), 1000);
	// --------------------------------------------------------------
	pMainWindow->signal_delete_event().connect(sigc::mem_fun(*static_cast<class MainWindow*>(this), &MainWindow::on_destroy));
	pMainWindow->set_resizable(false);
	// отключаем обработку NumLock в X-ах!!!
	// (см. описание в файле SetNumLock.cc)
	setNumLock(false, true);
	// --------------------------------------------------------------
	sp->write(70);
	// --------------------------------------------------------------
	pMainWindow->hide();
	// --------------------------------------------------------------
	sp->write(80);
	// --------------------------------------------------------------

	if( uniset::findArgParam("--maximize", conf->getArgc(), conf->getArgv()) != -1 )
	{
		pMainWindow->move(0, 0);
		pMainWindow->maximize();
		pMainWindow->fullscreen();
	}

	bm = new UWidgets("BigMnemo", shm, GUIConf::GladeXmlInstance(), guixml);
	static struct timeval tend;
	endTime( cout, tbeg, tend );
	sp->write(100);
	pMainWindow->show();
	//Установка времени двойного клика в 1/2 секунды
	Glib::RefPtr<Gdk::Display> disp = Gdk::Display::get_default();
	disp->set_double_click_time(500);
	waiting_for_mount = 0;
	btnJournalall->set_active(1);
	notebookjrn->set_current_page(1);

}
// -------------------------------------------------------------------------
bool MainWindow::btnShow_clicked(GdkEvent* event)
{
	GdkEventButton event_btn = event->button;

	if((event_btn.type == GDK_2BUTTON_PRESS))
	{
		pFullScreenSheme->show();
	}

	return false;
}
// -------------------------------------------------------------------------
void MainWindow::btnExit_clicked()
{
	pFullScreenSheme->hide();
}
// -------------------------------------------------------------------------
MainWindow::~MainWindow()
{
	connTime.disconnect();
	delete sp;
	delete ewin;
	delete winClock;
	delete flash;
}
// -------------------------------------------------------------------------
bool MainWindow::on_destroy( GdkEventAny* evnt )
{
	setNumLock(false, true);
	return true;
}
// -------------------------------------------------------------------------
void MainWindow::on_new_warning(const Gtk::TreeModel::iterator&)
{
	if( dlog.debugging(Debug::INFO) )
		dlog[Debug::INFO] << myname << ": new warning event.." << endl;
}
// -------------------------------------------------------------------------
void MainWindow::btnDate_clicked()
{
	winClock->set_position(Gtk::WIN_POS_CENTER);
	winClock->set_keep_above(true);
	winClock->set_now();
	string tm = winClock->run_clock_dialog();
}
// -------------------------------------------------------------------------
void MainWindow::btnMT_clicked()
{
	notebook->set_current_page(3);
	journal->hide();
	notebook->show();
}
// -------------------------------------------------------------------------
void MainWindow::btnState_clicked()
{
	notebook->set_current_page(0);
	journal->hide();
	notebook->show();
	sta->defaultState();
}
// -------------------------------------------------------------------------
void MainWindow::btnControl_clicked()
{
	notebook->set_current_page(1);
	journal->hide();
	notebook->show();
}
// -------------------------------------------------------------------------
void MainWindow::btnSetting_clicked()
{
	notebook->set_current_page(2);
	journal->hide();
	notebook->show();
	ewin->win_activate();
}
// -------------------------------------------------------------------------
void MainWindow::btnJournal_clicked()
{
	btnJournalnow->set_active(1);
	journal->show();
	notebookjrn->show();
	notebook->hide();
	jwin->win_activate();
	wwin->show();
	jwin->show();
}
// -------------------------------------------------------------------------
void MainWindow::btnJournalAll_clicked()
{
	journal->show();
	notebookjrn->set_current_page(0);
	wwin->show();
}
// -------------------------------------------------------------------------
void MainWindow::btnEnwinExit_clicked()
{
	cout << "btnEnwin Exit clicked" << endl;
	ewin->setMainPage();
}

// -------------------------------------------------------------------------
void MainWindow::btnJournalNow_clicked()
{
	journal->show();
	notebookjrn->set_current_page(1);
	jwin->show();
}
// -------------------------------------------------------------------------
void MainWindow::btnJournalRem_clicked()
{
	journal->show();
	notebookjrn->set_current_page(3);
}
// -------------------------------------------------------------------------
void MainWindow::btncooling_clicked()
{
	notebookbm->set_current_page(1);
	hide_label();
	tablebtn->set_sensitive(false);
}

void MainWindow::btnbackcooling_clicked()
{
	notebookbm->set_current_page(0);
	tablebtn->set_sensitive(true);
}

void MainWindow::btnU_clicked()
{
	hide_label();
	fixU->show();
}

void MainWindow::btnI_clicked()
{
	hide_label();
	fixI->show();
}

void MainWindow::btnP_clicked()
{
	hide_label();
	fixP->show();
}

void MainWindow::btnT_clicked()
{
	hide_label();
	fixT->show();
}

void MainWindow::hide_label()
{
	fixU->hide();
	fixI->hide();
	fixP->hide();
	fixT->hide();
}
// -------------------------------------------------------------------------
string MainWindow::saveSensors()
{
	GUIXml_ref guixml = GUIConf::GUIXmlInstance();
	xmlNode* cnode = guixml->findNode(guixml->getFirstNode(), "FlashLed");

	if( cnode == NULL )
		throw SystemError( "Not find conf-node for FlashLed" );

	UniXML_iterator it(cnode);
	string flash_script = it.getProp("flash_script");

	if( flash_script.empty() )
	{
		Gtk::MessageDialog msg(_("Не задана команда для сохранения на флэш-диск"), false, Gtk::MESSAGE_WARNING);
		msg.set_modal( true );
		msg.set_position( Gtk::WIN_POS_CENTER );
		msg.run();
		return "";
	}

	return "";
}
// -------------------------------------------------------------------------
bool MainWindow::setTime()
{
	static char buf[30];
	static time_t GMTime;
	static struct tm* tms;
	memset(buf, 0, sizeof(buf));
	GMTime = time(NULL);
	tms = localtime(&GMTime);
	strftime(buf, sizeof(buf), "%T", tms);
	lblTime->set_text(Glib::ustring(buf));
	return true;
}
// -------------------------------------------------------------------------
bool MainWindow::setDate()
{
	static char buf[30];
	static time_t GMTime;
	static struct tm* tms;
	memset(buf, 0, sizeof(buf));
	GMTime = time(NULL);
	tms = localtime(&GMTime);
	strftime(buf, sizeof(buf), "%d/%m/%Y", tms);
	lblDate->set_text(Glib::ustring(buf));
	return true;
}
// -------------------------------------------------------------------------
void MainWindow::poll()
{
	try
	{
		wwin->poll();
	}
	catch( ... ) {}

	try
	{
		//wwinremote->poll();
	}
	catch( ... ) {}

	try
	{
		//wwinlocal->poll();
	}
	catch( ... ) {}

	try
	{
		if (ewin->visible)
			ewin->poll();
	}
	catch( ... ) {}

	try
	{
		jwin->poll();
	}
	catch( ... ) {}

	try
	{
		awin->poll();
	}
	catch( ... ) {}

	try
	{
		flash->poll();
	}
	catch( ... ) {}

	try
	{
		sta->poll();
	}
	catch( ... ) {}

	try
	{
		bm->poll();
	}
	catch( ... ) {}

}
// -------------------------------------------------------------------------
void MainWindow::initIterators()
{
	cerr << myname << "(initIterators): *******" << endl;
	cerr << myname << "(initIterators): EWIN..." << endl;
	ewin->initIterators();
	cerr << myname << "(initIterators): WWIN..." << endl;
	wwin->initIterators();
	cerr << myname << "(initIterators): WWINREMOTE..." << endl;
	//wwinremote->initIterators();
	cerr << myname << "(initIterators): WWINLOCAL..." << endl;
	//wwinlocal->initIterators();
	cerr << myname << "(initIterators): JWIN..." << endl;
	jwin->initIterators();
	cerr << myname << "(initIterators): AWIN..." << endl;
	awin->initIterators();
	cerr << myname << "(initIterators): STA..." << endl;
	sta->initIterators();
	cerr << myname << "(initIterators): FLASH..." << endl;
	flash->initIterators();
	cerr << myname << "(initIterators): UWIDGETS..." << endl;
	bm->initIterators();
	cout << "InitIteratorsMain OK" << endl;
}
// -------------------------------------------------------------------------
void MainWindow::sensorInfo( uniset::SensorMessage* sm )
{
}
// -------------------------------------------------------------------------
void MainWindow::setFlashButtonsBlock( bool state )
{
}
// -------------------------------------------------------------------------
bool MainWindow::getFlashButtonsBlock()
{
	return false;
}
// -------------------------------------------------------------------------
void MainWindow::on_showAlarmJournal(const string& code, const string& datetime, const string& numcon, const std::string& bitreg )
{
	notebookjrn->set_current_page(2);
	awin->showPage(code, datetime, numcon );
	awin->show();
}
// -------------------------------------------------------------------------
void MainWindow::on_showAlarmWarning(const string& code, const string& datetime, const string& numcon, const std::string& bitreg )
{
	notebookjrn->set_current_page(2);
	awin->showPage(code, datetime, numcon );
	awin->show();
}
// -------------------------------------------------------------------------
