#include "State.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace MPU;
using namespace uniset;
// -------------------------------------------------------------------------
State::State(const string& name, const string& prefix, std::shared_ptr<uniset::SMInterface>& shm, BaseMainWindow* mwin):
	mainframe(0),
	tableState(0),
	drive1(0),
	fqc1(0),
	lsu1(0),
	trans1(0),
	trans2(0),
	drive2(0),
	fqc2(0),
	lsu2(0),
	trans3(0),
	trans4(0),
	winDrive1(0),
	winFQC1(0),
	winLSU1(0),
	winTrans1(0),
	winTrans2(0),
	winDrive2(0),
	winFQC2(0),
	winLSU2(0),
	winTrans3(0),
	winTrans4(0),
	btnDrive1(0),
	btnFQC1(0),
	btnLSU1(0),
	btnTrans1(0),
	btnDrive2(0),
	btnFQC2(0),
	btnLSU2(0),
	btnTrans2(0),
	btnStateExit(0)
{
	auto conf = uniset_conf();
	ObjectId ID(DefaultObjectId);
	ID = conf->getObjectID(name);

	if( ID == uniset::DefaultObjectId )
	{
		cerr << "(main): идентификатор '" << name
			 << "' не найден в конф. файле!"
			 << " в секции " << conf->getObjectsSection() << endl;
	}

	if( ID == DefaultObjectId )
		throw SystemError("(State::init): Unknown ID for '" + name + "'");

	shared_ptr<UniXML> xml = conf->getConfXML();
	xmlNode* cnode = xml->findNode(xml->getFirstNode(), "State", name);

	if( cnode == NULL )
		throw SystemError("(State): Not found confnode for name='" + name + "'");

	// std::shared_ptr<State> sta =std::make_shared<State>(ID,cnode,prefix,shm);
	Glib::RefPtr<Gtk::Builder> refXml = GUIConf::GladeXmlInstance();
	drive1 	= new CommonVO("Drive_VO1", shm, refXml);
	fqc1 	= new CommonVO("FQC_VO1", shm, refXml);
	lsu1 	= new CommonVO("LSU_VO1", shm, refXml);
	trans1 	= new CommonVO("Trans_VO1", shm, refXml);
	trans3 	= new CommonVO("Trans_VO2", shm, refXml);
	drive2 	= new CommonVO("Drive_VO2", shm, refXml);
	fqc2 	= new CommonVO("FQC_VO2", shm, refXml);
	lsu2 	= new CommonVO("LSU_VO2", shm, refXml);
	trans2 	= new CommonVO("Trans_VO3", shm, refXml);
	trans4 	= new CommonVO("Trans_VO4", shm, refXml);
	GETWIDGET(tableState, "tableState");
	GETWIDGET(mainframe, "mainframe");
	GETWIDGET(fqclocal, "mainframe1");
	GETWIDGET(fqcremote, "mainframe2");
	GETWIDGET(fqcmenu, "fqcmenu1");
	/* Экран Состояния*/
	winDrive1 = new DoubleParamWindow("DriveParamWindow1", tableState, shm, mwin);
	winDrive1->hide();
	winFQC1 = new DoubleParamWindow("FQCParamWindow1", tableState, shm, mwin);
	winFQC1->hide();
	winLSU1 = new DeviceWindow("LSUViewParamList1", tableState, "LSUParam_VO1", shm);
	winLSU1->hide();
	winTrans1 = new DeviceWindow("TransViewParamList1", tableState, "TransParam_VO1", shm);
	winTrans1->hide();
	winTrans3 = new DeviceWindow("TransViewParamList3", tableState, "TransParam_VO3", shm);
	winTrans3->hide();
	winDrive2 = new DoubleParamWindow("DriveParamWindow2", tableState, shm, mwin);
	winDrive2->hide();
	winFQC2 = new DoubleParamWindow("FQCParamWindow2", tableState, shm, mwin);
	winFQC2->hide();
	winLSU2 = new DeviceWindow("LSUViewParamList2", tableState, "LSUParam_VO2", shm);
	winLSU2->hide();
	winTrans2 = new DeviceWindow("TransViewParamList2", tableState, "TransParam_VO2", shm);
	winTrans2->hide();
	winTrans4 = new DeviceWindow("TransViewParamList4", tableState, "TransParam_VO4", shm);
	winTrans4->hide();
	/* Состояние*/
	//Обработчик нажатия кнопка параметры Двигателем 1
	GETWIDGET(btnDrive1, "btnDrive");
	btnDrive1->signal_clicked().connect(sigc::mem_fun(*this, &State::btnDrive1_clicked));
	//Обработчик нажатия кнопка параметры ПЧ 1
	GETWIDGET(btnFQC1, "btnFQC");
	btnFQC1->signal_clicked().connect(sigc::mem_fun(*this, &State::btnFQC1_clicked));
	//Обработчик нажатия кнопка параметры ЛСУ 1
	GETWIDGET(btnLSU1, "btnLSU");
	btnLSU1->signal_clicked().connect(sigc::mem_fun(*this, &State::btnLSU1_clicked));
	//Обработчик нажатия кнопка параметры Трансформатор 1
	GETWIDGET(btnTrans1, "btnTrans1");
	btnTrans1->signal_clicked().connect(sigc::mem_fun(*this, &State::btnTrans1_clicked));
	//Обработчик нажатия кнопка параметры Двигателем 2
	GETWIDGET(btnDrive2, "btnDrive1");
	btnDrive2->signal_clicked().connect(sigc::mem_fun(*this, &State::btnDrive2_clicked));
	//Обработчик нажатия кнопка параметры ПЧ 2
	GETWIDGET(btnFQC2, "btnFQC1");
	btnFQC2->signal_clicked().connect(sigc::mem_fun(*this, &State::btnFQC2_clicked));
	//Обработчик нажатия кнопка параметры ЛСУ 2
	GETWIDGET(btnLSU2, "btnLSU1");
	btnLSU2->signal_clicked().connect(sigc::mem_fun(*this, &State::btnLSU2_clicked));
	//Обработчик нажатия кнопка параметры Трансформатор 2
	GETWIDGET(btnTrans2, "btnTrans2");
	btnTrans2->signal_clicked().connect(sigc::mem_fun(*this, &State::btnTrans2_clicked));
	//Обработчик нажатия кнопка параметры Трансформатор 3
	GETWIDGET(btnTrans3, "btnTrans3");
	btnTrans3->signal_clicked().connect(sigc::mem_fun(*this, &State::btnTrans3_clicked));
	//Обработчик нажатия кнопка параметры Трансформатор 4
	GETWIDGET(btnTrans4, "btnTrans4");
	btnTrans4->signal_clicked().connect(sigc::mem_fun(*this, &State::btnTrans4_clicked));
	//Обработчик нажатия кнопки Выход
	GETWIDGET(btnStateExit, "ctl_btn_StateExit");
	btnStateExit->signal_clicked().connect(sigc::mem_fun(*this, &State::btnStateExit_clicked));
	//Обработчик нажатия кнопки ТЕКУЩИЙ
	GETWIDGET(btnFQCLocal, "ctl_btn_fqc1");
	btnFQCLocal->signal_clicked().connect(sigc::mem_fun(*this, &State::btnFQCLocal_clicked));
	btnFQCLocal->set_active(true);
	//Обработчик нажатия кнопки УДАЛЕННЫЙ
	GETWIDGET(btnFQCRemote, "ctl_btn_fqc2");
	btnFQCRemote->signal_clicked().connect(sigc::mem_fun(*this, &State::btnFQCRemote_clicked));
}
// -------------------------------------------------------------------------
State::~State()
{
	delete drive1;
	delete fqc1;
	delete lsu1;
	delete trans1;
	delete drive2;
	delete fqc2;
	delete lsu2;
	delete trans2;
	delete trans3;
	delete trans4;
	delete winDrive1;
	delete winLSU1;
	delete winTrans1;
	delete winDrive2;
	delete winLSU2;
	delete winTrans2;
	delete winFQC1;
	delete winFQC2;
}
// -------------------------------------------------------------------------
void State::btnDrive1_clicked()
{
	State::readyState();
	winDrive1->win_activate();
}
// -------------------------------------------------------------------------
void State::btnFQC1_clicked()
{
	State::readyState();
	winFQC1->win_activate();
}
// -------------------------------------------------------------------------
void State::btnLSU1_clicked()
{
	State::readyState();
	winLSU1->win_activate();
}
// -------------------------------------------------------------------------
void State::btnTrans1_clicked()
{
	State::readyState();
	winTrans1->win_activate();
}
// -------------------------------------------------------------------------
void State::btnDrive2_clicked()
{
	State::readyState();
	winDrive2->win_activate();
}
// -------------------------------------------------------------------------
void State::btnFQC2_clicked()
{
	State::readyState();
	winFQC2->win_activate();
}
// -------------------------------------------------------------------------
void State::btnLSU2_clicked()
{
	State::readyState();
	winLSU2->win_activate();
}
// -------------------------------------------------------------------------
void State::btnTrans2_clicked()
{
	State::readyState();
	winTrans2->win_activate();
}
// -------------------------------------------------------------------------
void State::btnTrans3_clicked()
{
	State::readyState();
	winTrans3->win_activate();
}
// -------------------------------------------------------------------------
void State::btnTrans4_clicked()
{
	State::readyState();
	winTrans4->win_activate();
}
// -------------------------------------------------------------------------
void State::btnStateExit_clicked()
{
	mainframe->show();
	fqcmenu->show();
	tableState->hide();
}
// -------------------------------------------------------------------------
void State::btnFQCLocal_clicked()
{
	fqclocal->show();
	fqcremote->hide();
}
// -------------------------------------------------------------------------
void State::btnFQCRemote_clicked()
{
	fqcremote->show();
	fqclocal->hide();
}
// -------------------------------------------------------------------------
void State::readyState()
{
	mainframe->hide();
	tableState->show();
	winTrans1->hide();
	winTrans3->hide();
	winTrans4->hide();
	winFQC1->hide();
	winFQC2->hide();
	winLSU1->hide();
	winDrive1->hide();
	winTrans2->hide();
	winLSU2->hide();
	winDrive2->hide();
	fqcmenu->hide();
}
// -------------------------------------------------------------------------
void State::initIterators()
{
	drive1->initIterators();
	fqc1->initIterators();
	lsu1->initIterators();
	trans1->initIterators();
	trans3->initIterators();
	trans4->initIterators();
	drive2->initIterators();
	fqc2->initIterators();
	lsu2->initIterators();
	trans2->initIterators();
	winDrive1->initIterators();
	winFQC1->initIterators();
	winLSU1->initIterators();
	winTrans1->initIterators();
	winDrive2->initIterators();
	winFQC2->initIterators();
	winLSU2->initIterators();
	winTrans2->initIterators();
	winTrans3->initIterators();
	winTrans4->initIterators();
}
// -------------------------------------------------------------------------
void State::poll()
{
	drive1->poll();
	fqc1->poll();
	lsu1->poll();
	trans1->poll();
	trans2->poll();
	trans3->poll();
	trans4->poll();
	drive2->poll();
	fqc2->poll();
	lsu2->poll();
	if(winDrive1->visible) winDrive1->poll();
	if(winFQC1->visible) winFQC1->poll();
	if(winLSU1->visible) winLSU1->poll();
	if(winTrans1->visible) winTrans1->poll();
	if(winDrive2->visible) winDrive2->poll();
	if(winFQC2->visible) winFQC2->poll();
	if(winLSU2->visible) winLSU2->poll();
	if(winTrans2->visible) winTrans2->poll();
	if(winTrans3->visible) winTrans3->poll();
	if(winTrans4->visible) winTrans4->poll();
}
// -------------------------------------------------------------------------
void State::getList(ParamList::ValList* lst)
{
	if(winDrive1->visible) winDrive1->getList(lst);
	if(winFQC1->visible) winFQC1->getList(lst);
	if(winLSU1->visible) winLSU1->getList(lst);
	if(winTrans1->visible) winTrans1->getList(lst);
	if(winDrive2->visible) winDrive2->getList(lst);
	if(winFQC2->visible) winFQC2->getList(lst);
	if(winLSU2) winLSU2->getList(lst);
	if(winTrans2->visible) winTrans2->getList(lst);
	if(winTrans3->visible) winTrans3->getList(lst);
	if(winTrans4->visible) winTrans4->getList(lst);
}
// -------------------------------------------------------------------------
void State::init_first_time()
{
	drive1->init_first_time();
	fqc1->init_first_time();
	lsu1->init_first_time();
	trans1->init_first_time();
	trans2->init_first_time();
	trans3->init_first_time();
	trans4->init_first_time();
	drive2->init_first_time();
	fqc2->init_first_time();
	lsu2->init_first_time();
	winDrive1->init_first();
	winFQC1->init_first();
	winLSU1->init_first();
	winTrans1->init_first();
	winDrive2->init_first();
	winFQC2->init_first();
	winLSU2->init_first();
	winTrans2->init_first();
	winTrans3->init_first();
	winTrans4->init_first();
}
// -------------------------------------------------------------------------
void State::defaultState()
{
	mainframe->show();
	tableState->hide();
	fqcmenu->show();
}
// -------------------------------------------------------------------------
