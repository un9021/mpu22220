#!/bin/sh

INFILE=$1

SEP=';'

OLDIFS=$IFS
IFS=$SEP

[ ! -f $INFILE ] && { echo "Unknown file. Use ${0##*/} file.csv"; exit 1; }

#Регистр;Бит;Номер в Базе;Событие;Расположение;Описание события;Рекомендация №1;Схема;Рекомендация №2;Схема;Рекомендация №3;Схема;Рекомендация №4;Схема;Рекомендация №5;Схема;Рекомендация №6;Схема;Рекомендация №7;Схема

TFILE=`mktemp`

cat $INFILE | tail -n +2 | iconv -f cp1251 -t utf8 -o $TFILE && dos2unix $TFILE

# Генерируем начало файла
echo '<?xml version="1.0" encoding="utf-8"?>'
echo '<alarmlist>'

REG=
while read nreg nbit num type src desc r1 s1 r2 s2 r3 s3 r4 s4 r5 s5 r6 s6 r7 s7 r8 s8 r9 s9 r10 s10
do
    [ -n "$nreg" ] && REG="$nreg" && continue
	[ -z "$nbit" ] && continue

	# удаляем лишние пробелы (>2).. чтобы отсечь пустые строки
	desc=`echo "$desc" | sed -e "s|([[:space:]){,2}]||g"`
	type=`echo "$type" | sed -e "s|([[:space:]){,2}]||g"`

	[ -z "$desc" ] && [ -z "$type" ] && continue

	# заменяем символы ">,<,&" т.к. их нельзя использовать в xml в "прямом виде"
    src=`echo "$src" | sed -e "s|\&|\&amp;|g" | sed -e "s|<|\&lt;|g" -e "s|>|\&gt;|g"`
    desc=`echo "$desc" | sed -e "s|\&|\&amp;|g" | sed -e "s|<|\&lt;|g" -e "s|>|\&gt;|g"`

    echo "  <alarm code=\"$num\" reg=\"$REG.$nbit\">"
    if echo "$type" | grep -qi "штат"; then
	    echo "    <title type=\"warning\">Нештатная ситуация $num</title>"
    elif echo "$type" | grep -qi "инфор"; then
	    echo "    <title type=\"info\">Информация $num</title>"
    else
	    echo "    <title type=\"alarm\">Авария $num</title>"
    fi

   	echo "    <source>${src}</source>"
    echo "    <description>${desc}</description>"
    echo "    <recommendation>"

    IFS=$OLDIFS
    for N in `seq 1 10`; 
    do
       rec=
       shm=
       eval rec=\$r${N}
       eval shm=\$s${N}

       [ -z "$rec" ] && continue

		# заменяем символы ">,<,&" т.к. их нельзя использовать в xml в "прямом виде"
        rec=`echo "$rec" | sed -e "s|\&|\&amp;|g" | sed -e "s|<|\&lt;|g" -e "s|>|\&gt;|g"`
        shm=`echo "$shm" | sed -e "s|\&|\&amp;|g" | sed -e "s|<|\&lt;|g" -e "s|>|\&gt;|g"`


       if [ -z "$shm" ]; then 
            echo "      <item title=\"Рекомендация N$N\">${rec}</item>"
       else
            echo "      <item title=\"Рекомендация N$N\">${rec}"
            echo "Схема: ${shm}</item>"
       fi
    done
    IFS=$SEP

    echo "    </recommendation>"
    echo "  </alarm>"

done<$TFILE

IFS=$OLDIFS

# Конец файла
echo '</alarmlist>'

rm -f $TFILE
