#include "Control.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace MPU;
using namespace uniset;
// -------------------------------------------------------------------------
shared_ptr<Control> Control::create(const string &name, std::shared_ptr<uniset::SMInterface> &shm, const string &prefix)
{
	auto conf = uniset_conf();
	ObjectId ID(DefaultObjectId);
	ID = conf->getObjectID(name);

	if (ID == uniset::DefaultObjectId)
	{
		cerr << "(main): идентификатор '" << name
			 << "' не найден в конф. файле!"
			 << " в секции " << conf->getObjectsSection() << endl;
	}

	if (ID == DefaultObjectId)
		throw SystemError("(Control::init): Unknown ID for '" + name + "'");

	shared_ptr<UniXML> xml = conf->getConfXML();
	xmlNode *cnode = xml->findNode(xml->getFirstNode(), "Control", name);

	if (cnode == NULL)
		throw SystemError("(Control): Not found confnode for name='" + name + "'");

	shared_ptr<Control> ctl = make_shared<Control>(ID, cnode, shm, prefix);
	auto act = UniSetActivator::Instance();
	act->add(ctl);
	return ctl;
}
// -------------------------------------------------------------------------
Control::Control(uniset::ObjectId id, xmlNode *cnode, std::shared_ptr<uniset::SMInterface> &shm, const std::string &prefix) : UObjectT<Control_SK>(id, cnode, prefix),
																															  btnRPM(0),
																															  setPower1(0),
																															  setPower2(0)
{
	uconf = UserConf::Instance();
	dlgPass = PasswordDialog::Instance();
	Glib::RefPtr<Gtk::Builder> refXml = GUIConf::GladeXmlInstance();
	UniXML::iterator it(cnode);
	dlgSetValue = SetValueDialog::Instance();
	dlgSetValue->show_btnMinus();
	cmdTimeout = it.getPIntProp("cmdTimeout", 3000);
	//Обработчик нажатия кнопки Здадание по W
	GETWIDGET(btnVSetting, "ctl_btn_w");
	btnVSetting->signal_clicked().connect(sigc::mem_fun(*this, &Control::btnVSetting_clicked));
	//Обработчик нажатия кнопки Здадание по P
	GETWIDGET(btnPSetting, "ctl_btn_P");
	btnPSetting->signal_clicked().connect(sigc::mem_fun(*this, &Control::btnPSetting_clicked));
	//Обработчик октрытия выставление задачи оборотов
	GETWIDGET(btnRPM, "ctl_btn_RPM");
	btnRPM->signal_clicked().connect(sigc::mem_fun(*this, &Control::btnSetRPM_clicked));
	//Обработчик нажатия кнопки Задание мощности ПЧ1
	GETWIDGET(setPower1, "ctl_btn_FQC1");
	setPower1->signal_clicked().connect(sigc::mem_fun(*this, &Control::btnsetPower1_clicked));
	//Обработчик нажатия кнопки Задание мощности ПЧ2
	GETWIDGET(setPower2, "ctl_btn_FQC2");
	setPower2->signal_clicked().connect(sigc::mem_fun(*this, &Control::btnsetPower2_clicked));
	GETWIDGET(btnMT, "btnMT");
	btnMT->signal_clicked().connect(sigc::mem_fun(*this, &Control::btnMT_clicked));

	GETWIDGET(lbl, "lbl_zadan");
	GETWIDGET(setFQC, "SetFQC");
	GETWIDGET(mnemobook, "notebook2");
	GETWIDGET(fxMT, "fxMT");
	GETWIDGET(fxInd, "fxInd");
	/* по-умолчанию блокируем задание оборотов и мощности*/
	blockcontrol(false);
	/* по-умолчанию режим задания неопределен*/
	vset();
	mnemoset(true); //по-умолчанию выставляем одинарную мнемосхему
	out_SettingMode = fcmUndef;
}
// -------------------------------------------------------------------------
Control::~Control()
{
}
// -------------------------------------------------------------------------
void Control::step()
{
	(!(in_RemoteControl) && in_MasterSlave) ? blockcontrol(true) : blockcontrol(false);

	//по-умолчанию выставляем одинарную мнемосхему
	//	(in_RemoteControl) ? mnemoset(true) : mnemoset(false);
	//	if (in_ISettingMode == fcmUndef)
	//		out_SettingMode = fcmUndef;

	if (in_Mode == fcAlarm)
	{
		out_GED_TargetRPM_AS = 0;
		out_SetTargetPowerFQC1_AS = 0;
		out_SetTargetPowerFQC2_AS = 0;
	}

	if ((!(in_RemoteControl) && !(in_MasterSlave)) || in_RemoteControl)
	{
		blockcontrol(false);

		if (in_ISettingMode == 1)
		{
			vset();
		}
		else if (in_ISettingMode == 2)
		{
			pset();
		}
	}

	if (in_TargetMT == out_ConfirmMT) 
	{
		btnMT->set_sensitive(false);
		system("snmpset -v 2c -c comm_rw mtcall_r1 1.3.6.1.4.1.38838.3.2.1.1.3 i 2");
		system("snmpset -v 2c -c comm_rw mtcall_r2 1.3.6.1.4.1.38838.3.2.1.1.3 i 2");
	}
	else
	{
		btnMT->set_sensitive(true);
		system("snmpset -v 2c -c comm_rw mtcall_r1 1.3.6.1.4.1.38838.3.2.1.1.3 i 1");
		system("snmpset -v 2c -c comm_rw mtcall_r2 1.3.6.1.4.1.38838.3.2.1.1.3 i 1");
	}

	if (in_isOn && in_ControlFQC && in_MasterSlave)
	{
		fxMT->set_visible(true);
		fxInd->set_visible(false);

		if (in_TargetMT == out_ConfirmMT) 
		{
		btnMT->set_sensitive(false);
		system("snmpset -v 2c -c comm_rw mtcall_r1 1.3.6.1.4.1.38838.3.2.1.1.3 i 2");
		system("snmpset -v 2c -c comm_rw mtcall_r2 1.3.6.1.4.1.38838.3.2.1.1.3 i 2");
		}
		else
		{
		btnMT->set_sensitive(true);
		system("snmpset -v 2c -c comm_rw mtcall_r1 1.3.6.1.4.1.38838.3.2.1.1.3 i 1");
		system("snmpset -v 2c -c comm_rw mtcall_r2 1.3.6.1.4.1.38838.3.2.1.1.3 i 1");
		}
	}
	else
	{
		fxMT->set_visible(false);
		fxInd->set_visible(true);
	}
}
// -------------------------------------------------------------------------
void Control::blockcontrol(bool par)
{
	if (in_ISettingMode == fcmUndef && par)
	{
		btnRPM->set_sensitive(false);
		setPower1->set_sensitive(false);
		setPower2->set_sensitive(false);
	}
	else
	{
		btnRPM->set_sensitive(par);
		setPower1->set_sensitive(par);
		setPower2->set_sensitive(par);
	}

	btnVSetting->set_sensitive(par);
	btnPSetting->set_sensitive(par);
}
// -------------------------------------------------------------------------
void Control::mnemoset(bool set)
{
	//mnemobook->set_current_page(int(set));
	//по-умолчанию выставляем одинарную мнемосхему
	mnemobook->set_current_page(1);
}
// -------------------------------------------------------------------------
// преобразование текста (по сути с игнорированием LOCALE)
// заменяет "," --> "." чтобы atof корректно сработала
static float str2F(const string &ss)
{
	string s(ss);
	string::size_type pos = s.find(",");

	while (pos != string::npos)
	{
		s.replace(pos, 1, ".");
		pos = s.find(",");
	}

	return atof(s.c_str());
}
// -------------------------------------------------------------------------
void Control::btnVSetting_clicked()
{
	vset();
}
// -------------------------------------------------------------------------
void Control::btnPSetting_clicked()
{
	pset();
}
// -------------------------------------------------------------------------
void Control::pset()
{
	out_SettingMode = fcmSetPower; //согласовано с ПЧ 2=режим задания по мощности

	out_SetTargetPowerFQC1_AS = in_Power;
	out_SetTargetPowerFQC2_AS = in_Power;
	lbl->set_label("Задание по P, MВт:");
	btnRPM->hide();
	setFQC->show();
}
// -------------------------------------------------------------------------
void Control::vset()
{
	out_SettingMode = fcmSetRPM; //согласовано с ПЧ 1=режим задания по скорости

	out_GED_TargetRPM_AS = in_RPM;
	lbl->set_label("Задание на N, об/мин:");
	btnRPM->show();
	setFQC->hide();
}
// -------------------------------------------------------------------------
void Control::btnSetRPM_clicked()
{
	ostringstream sval;
	sval << in_RPM;
	string txt = dlgSetValue->run(sval.str(), "Задание оборотов, об/мин", -120, 120);
	float val = str2F(txt);
	out_GED_TargetRPM_AS = val;
}
// -------------------------------------------------------------------------
void Control::btnsetPower1_clicked()
{
	ostringstream sval;
	sval << in_Power / 1000;
	string txt = dlgSetValue->run(sval.str(), "Задание мощности, МВт", -10, 10);
	float val = str2F(txt);
	out_SetTargetPowerFQC1_AS = val * 1000;
}
// -------------------------------------------------------------------------
void Control::btnsetPower2_clicked()
{
	ostringstream sval;
	sval << in_Power / 1000;
	string txt = dlgSetValue->run(sval.str(), "Задание мощности, МВт", -10, 10);
	float val = str2F(txt);
	out_SetTargetPowerFQC2_AS = val * 1000;
}
// -------------------------------------------------------------------------
void Control::btnMT_clicked()
{
	out_ConfirmMT = in_TargetMT;
}
// -------------------------------------------------------------------------
void Control::sensorInfo(const uniset::SensorMessage *shm)
{
}