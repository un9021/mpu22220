// -------------------------------------------------------------------------
#pragma once
// -------------------------------------------------------------------------
#include <GUIConfiguration.h>
#include <SetValueDialog.h>
#include <PasswordDialog.h>
#include <UObjectT.h>
#include <uniset2/UniSetActivator.h>
#include "Control_SK.h"
#include <cstdlib>
// -------------------------------------------------------------------------
/*!	Реализация управления */
class Control:
	public Glib::Object,
	public UObjectT<Control_SK>
{
	public:
		Control( uniset::ObjectId id, xmlNode* node, std::shared_ptr<uniset::SMInterface>& shm,const std::string& prefix );
		virtual ~Control();
		
		static std::shared_ptr<Control> create( const std::string& name,std::shared_ptr<uniset::SMInterface>& shm, const std::string& prefix = "" );
		
	protected:
	virtual void sensorInfo( const uniset::SensorMessage* shm ) override;
		virtual void step();
		
		int cmdTimeout;
		
		Glib::RefPtr<MPU::SetValueDialog> dlgSetValue;
		Glib::RefPtr<MPU::UserConf> uconf;
		Glib::RefPtr<MPU::PasswordDialog> dlgPass;
		
		enum FCControlMode
		{
			fcmUndef    = 0,	/*!< неопределённое состояние (управление отключено?) */
			fcmSetRPM   = 1,	/*!< режим управления по оборотам */
			fcmSetPower = 2		/*!< режим управления по мощности */
		};
		
		enum fcState
		{
			fcUndefind = 0,		/*!< Неопределенное состояние*/
			fcReady1 = 1,		/*!< ГОТОВНОСТЬ1*/
			fcReady2 = 2,		/*!< ГОТОВНОСТЬ2*/
			fcStop = 3,		/*!< ОСТАНОВЛЕН*/
			fcWork = 4,		/*!< РАБОТА, ХОД, ВРАЩЕНИЕ*/
			fcTurning = 5,		/*!< ПРОВОРОТ ВАЛА*/
			fcAlarm = 6		/*!< АВАРИЯ*/
		};
		
		void btnMT_clicked();

		/* Открыть диалог задачи оборотов  */
		void btnSetRPM_clicked();
		
		/* Нажатие кнопки Задание по V*/
		void btnVSetting_clicked();
		/* Нажатие кнопки Задание по P*/
		void btnPSetting_clicked();
		
		/*Задание оборотов и мощности*/
		/* Нажатие кнопки Задание оборотов ГЭД*/
		void btnSetRPMGED_clicked();
		
		/* Нажатие кнопки Задание мощности ПЧ1*/
		void btnsetPower1_clicked();
		
		/* Нажатие кнопки Задание мощности ПЧ2*/
		void btnsetPower2_clicked();
		
		/* Блокировка/разблокировка управляющих кнопок*/
		void blockcontrol(bool par);
		
		/* Переключение маленькой мнемосхемы*/
		void mnemoset(bool set);
		
		/* Установка по мощности(кнопки появляются)*/
		void pset();
		
		/* Установка по скорости(кнопки появляются)*/
		void vset();
		
		
	private:
		Gtk::RadioButton* btnVSetting;
		Gtk::RadioButton* btnPSetting;
		Gtk::Button* btnRPM;
		Gtk::HBox* setFQC;
		Gtk::Button* setPower1;
		Gtk::Button* setPower2;
		Gtk::Label* lbl;
		Gtk::Label* lblmain;
		Gtk::Table* tablefqc;
		Gtk::HBox* targetrpm;
		Gtk::Fixed* fxMT;
		Gtk::Fixed* fxInd;
		Gtk::Notebook* mnemobook;
		Gtk::Button* btnMT;
		
};
// -------------------------------------------------------------------------
