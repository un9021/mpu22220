#!/bin/sh
rm -f mpu22220-gui.ui
make mpu22220-gui.ui
export PATH="${PATH}:./:../../libs/:../../libs/.libs/"

#ulimit -Sc 10000000
START=uniset2-start.sh
export GTK2_RC_FILES=./gtkrc 
export LC_ALL=ru_RU.UTF-8 

${START} -f ./mpu22220-gui \
	--smemory-id SharedMemoryGUI \
	--mpu-guifile mpu22220-gui.ui \
	--mpu-guiconfile guiconfigure.xml \
	--pass-check-off \
	--no-beep \
	--dlog-add-levels crit \
	--unideb-add-levels crit \
	--pulsar-id Pulsar_S \
	--pulsar-msec 5000 \
	--e-filter evnt_gui \
	--mbtcp1-name MBTCPMultiMaster1 \
	--mbtcp1-gateway-iaddr localhost \
	--mbtcp1-gateway-port 2050 \
	--mbtcp1-filter-field tcpgui \
	--mbtcp1-filter-value MPU1 \
	--mbtcp1-set-prop-prefix mbgui_ \
	--mbtcp2-name MBTCPMultiMaster2 \
	--mbtcp2-gateway-iaddr localhost \
	--mbtcp2-gateway-port 2051 \
	--mbtcp2-filter-field tcpgui \
	--mbtcp2-filter-value MPU2 \
	--mbtcp2-set-prop-prefix mbgui_ \
	--mbplc-name MBTCPMultiMasterPLC \
	--mbplc-gateway-iaddr localhost \
	--mbplc-gateway-port 5000 \
	--mbplc-filter-field tcp \
	--mbplc-filter-value gedplc \
	--mbplc-set-prop-prefix  \
	--mbplcprot-name MBTCPMultiMasterPLCPROT \
	--mbplcprot-gateway-iaddr localhost \
	--mbplcprot-gateway-port 5001 \
	--mbplcprot-filter-field tcp \
	--mbplcprot-filter-value gedprot \
	--mbplcprot-set-prop-prefix  \

	$*
killall get-gate-version.sh
