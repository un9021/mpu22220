// -------------------------------------------------------------------------
#pragma once
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <GUIConfiguration.h>
#include <BaseMainWindow.h>
#include <CommonVO.h>
#include <ParamList.h>
#include <uniset2/UniSetActivator.h>
#include "DeviceWindow.h"
#include "DoubleParamWindow.h"
#include "ParamList.h"
// -------------------------------------------------------------------------
using namespace std;
// -------------------------------------------------------------------------

/*!	Реализация управления */
class State:
	public Glib::Object
{
	public:
		State( const string& name, const string& prefix, std::shared_ptr<uniset::SMInterface>& shm, MPU::BaseMainWindow* mwin);
		virtual ~State();
		
		void initIterators();
		virtual void poll();
		
		/* Возвращаем в исходной состояние экран */
		void defaultState();
		
		void getList(MPU::ParamList::ValList* lst);
	protected:
		virtual void init_first_time();
		
		Gtk::HBox* mainframe;
		Gtk::VBox* tableState;
		Gtk::VBox* fqclocal;
		Gtk::VBox* fqcremote;
		Gtk::HBox* fqcmenu;
		
		MPU::CommonVO* drive1; 	/*!< параметры двигателя */
		MPU::CommonVO* fqc1;		/*!< параметры преобразователя */
		MPU::CommonVO* lsu1;		/*!< параметры ЛСУ */
		MPU::CommonVO* trans1;	/*!< параметры трансформатора */
		MPU::CommonVO* trans3;	/*!< параметры трансформатора */
		MPU::CommonVO* drive2; 	/*!< параметры двигателя */
		MPU::CommonVO* fqc2;		/*!< параметры преобразователя */
		MPU::CommonVO* lsu2;		/*!< параметры ЛСУ */
		MPU::CommonVO* trans2;	/*!< параметры трансформатора */
		MPU::CommonVO* trans4;	/*!< параметры трансформатора */
		
		MPU::DoubleParamWindow* winDrive1; 	/*!< окно текущих параметров двигателя */
		MPU::DoubleParamWindow* winFQC1; 	/*!< окно текущих параметров преобразователя */
		MPU::DeviceWindow* winLSU1; 		/*!< окно текущих параметров ЛСУ */
		MPU::DeviceWindow* winTrans1; 	/*!< окно текущих параметров Трансформатора TV11*/
		MPU::DeviceWindow* winTrans3; 	/*!< окно текущих параметров Трансформатора TV12*/
		MPU::DoubleParamWindow* winDrive2; 	/*!< окно текущих параметров двигателя */
		MPU::DoubleParamWindow* winFQC2; 	/*!< окно текущих параметров преобразователя */
		MPU::DeviceWindow* winLSU2; 		/*!< окно текущих параметров ЛСУ */
		MPU::DeviceWindow* winTrans2; 	/*!< окно текущих параметров Трансформатора TV21*/
		MPU::DeviceWindow* winTrans4; 	/*!< окно текущих параметров Трансформатора TV22*/
		
		/*Состояние*/
		/*! Нажатие параметры Двигателя 1*/
		void btnDrive1_clicked();
		
		/* Нажатие параметры ПЧ */
		void btnFQC1_clicked();
		
		/* Нажатие параметры ЛСУ */
		void btnLSU1_clicked();
		
		/* Нажатие параметры Трансформатора */
		void btnTrans1_clicked();
		
		/*! Нажатие параметры Двигателя 2*/
		void btnDrive2_clicked();
		
		/* Нажатие параметры ПЧ */
		void btnFQC2_clicked();
		
		/* Нажатие параметры ЛСУ */
		void btnLSU2_clicked();
		
		/* Нажатие параметры Трансформатора */
		void btnTrans2_clicked();
		
		/* Нажатие параметры Трансформатора */
		void btnTrans3_clicked();
		
		/* Нажатие параметры Трансформатора */
		void btnTrans4_clicked();
		
		/* Нажатие кнопки Выход */
		void btnStateExit_clicked();
		
		/* Нажатие кнопки ТЕКУЩИЙ */
		void btnFQCLocal_clicked();
		
		/* Нажатие кнопки УДАЛЕННЫЙ */
		void btnFQCRemote_clicked();
		
		/* Готовим место */
		void readyState();
		
	private:
		Gtk::Button* btnDrive1; //кнопка параметры Двигателем
		Gtk::Button* btnFQC1; //кнопка параметры ПЧ
		Gtk::Button* btnLSU1; //кнопка параметры ЛСУ
		Gtk::Button* btnTrans1; //кнопка параметры Трансформатор
		Gtk::Button* btnDrive2; //кнопка параметры Двигателем
		Gtk::Button* btnFQC2; //кнопка параметры ПЧ
		Gtk::Button* btnLSU2; //кнопка параметры ЛСУ
		Gtk::Button* btnTrans2; //кнопка параметры Трансформатор
		Gtk::Button* btnStateExit; //кнопка выхода на экран Состояния
		Gtk::RadioButton* btnFQCLocal; //кнопка выхода на экран ТЕКУЩИЙ
		Gtk::RadioButton* btnFQCRemote; //кнопка выхода на экран УДАЛЕННЫЙ
		Gtk::Button* btnTrans3; //кнопка параметры Трансформатор
		Gtk::Button* btnTrans4; //кнопка параметры Трансформатор
};
// -------------------------------------------------------------------------
