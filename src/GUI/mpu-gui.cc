#include "MainWindow.h"
#include <Exceptions.h>
#include <GUIConfiguration.h>
#include <MPUConfiguration.h>
#include <MPUSharedMemory.h>
#include <UniSetTypes.h>
#include <UniXML.h>
#include <extensions/Extensions.h>
#include <extensions/MBTCPMultiMaster.h>
#include <gtkmm.h>
#include <uniset2/UniSetActivator.h>
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
using namespace MPU;
// -------------------------------------------------------------------------
extern void setNumLock(bool state, bool no_emit = false);
// -------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	Gtk::Main kit(argc, argv);
	setlocale(LC_NUMERIC, "C");

	try
	{
		uniset_init(argc, (const char **)(argv));
		auto conf = uniset_conf();
		string logfilename(conf->getArgParam("--logfile", "mpu3-gui.log"));
		string logname(conf->getLogDir() + logfilename);
		dlog.logFile(logname.c_str());
		conf->initLogStream(dlog, "dlog");
		ObjectId ID(DefaultObjectId);
		string name = conf->getArgParam("--name", "GUIManager1");
		ID = conf->getObjectID(name);

		if (ID == DefaultObjectId)
		{
			cerr << "(main): идентификатор '" << name
				 << "' не найден в конф. файле!"
				 << " в секции " << conf->getObjectsSection() << endl;
			return 0;
		}

		bool skip_mb1 = findArgParam("--skip-mbtcp1", argc, argv) != -1;
		bool skip_mb2 = findArgParam("--skip-mbtcp2", argc, argv) != -1;
		bool skip_mbplc = findArgParam("--skip-mbplc", argc, argv) != -1;
		bool skip_mbplcprot = findArgParam("--skip-mbplcprot", argc, argv) != -1;
		bool skip_mbplcmt = findArgParam("--skip-mbplcmt", argc, argv) != -1;
		
		auto gact = UniSetActivator::Instance();
		auto shm = MPUSharedMemory::init_mpusmemory(argc, argv);

		if (!shm)
			return 1;

		gact->add(shm);
		std::shared_ptr<IONotifyController> ioc = static_pointer_cast<IONotifyController>(shm);
		Glib::RefPtr<MainWindow> win = MainWindow::init_gui(ID, argc, argv, ioc);

		if (!skip_mb1)
		{
			std::shared_ptr<MBTCPMultiMaster> mbm1 = MBTCPMultiMaster::init_mbmaster(argc, argv, shm->getId(), shm, "mbtcp1");

			if (!mbm1)
				return 1;

			gact->add(mbm1);
		}

		if (!skip_mb2)
		{
			std::shared_ptr<MBTCPMultiMaster> mbm2 = MBTCPMultiMaster::init_mbmaster(argc, argv, shm->getId(), shm, "mbtcp2");

			if (!mbm2)
				return 1;

			gact->add(mbm2);
		}

		if (!skip_mbplc)
		{
			std::shared_ptr<MBTCPMultiMaster> mbplc = MBTCPMultiMaster::init_mbmaster(argc, argv, shm->getId(), shm, "mbplc");

			if (!mbplc)
				return 1;

			gact->add(mbplc);
		}

		if (!skip_mbplcprot)
		{
			std::shared_ptr<MBTCPMultiMaster> mbplcprot = MBTCPMultiMaster::init_mbmaster(argc, argv, shm->getId(), shm, "mbplcprot");

			if (!mbplcprot)
				return 1;

			gact->add(mbplcprot);
		}

		if (!skip_mbplcmt)
		{
			std::shared_ptr<MBTCPMultiMaster> mbplcmt = MBTCPMultiMaster::init_mbmaster(argc, argv, shm->getId(), shm, "mbplcmt");

			if (!mbplcmt)
				return 1;

			gact->add(mbplcmt);
		}

		SystemMessage sm(SystemMessage::StartUp);
		gact->broadcast(sm.transport_msg());
		gact->run(true);

		while (!shm->exist())
		{
			cerr << "...waiting activation..." << endl;
			msleep(1000);
		}

		cerr << "..activation...[OK]" << endl;
		string theme = getArgParam("--gtkthemedir", argc, argv, "");

		if (!theme.empty())
			gtk_rc_parse((theme + "/gtkrc").c_str());

		win->activate();
		Gtk::Main::run();
		return 0;
	}
	catch (SystemError &ex)
	{
		dlog[Debug::CRIT] << "(GUI::main): " << ex << endl;
	}
	catch (Exception &ex)
	{
		dlog[Debug::CRIT] << "(GUI::main): " << ex << endl;
	}
	catch (CORBA::NO_IMPLEMENT)
	{
		dlog[Debug::CRIT] << "(GUI:main): CORBA::NO_IMPLEMENT..." << endl;
	}
	catch (CORBA::OBJECT_NOT_EXIST)
	{
		dlog[Debug::CRIT] << "(GUI:main): CORBA::OBJECT_NOT_EXIST..." << endl;
	}
	catch (CORBA::COMM_FAILURE &ex)
	{
		dlog[Debug::CRIT] << "(GUI:main): CORBA::COMM_FAILURE..." << endl;
	}
	catch (CORBA::SystemException &ex)
	{
		dlog[Debug::CRIT] << "(GUI:main): (CORBA::SystemException): "
						  << ex.NP_minorString() << endl;
	}
	catch (omniORB::fatalException &fe)
	{
		dlog[Debug::CRIT] << "(GUI::main): поймали omniORB::fatalException:" << endl;
		dlog[Debug::CRIT] << "  file: " << fe.file() << endl;
		dlog[Debug::CRIT] << "  line: " << fe.line() << endl;
		dlog[Debug::CRIT] << "  mesg: " << fe.errmsg() << endl;
	}
	catch (Glib::Error &ex)
	{
		dlog[Debug::CRIT] << "(GUI::main): " << ex.what() << endl;
	}
	catch (std::exception &ex)
	{
		dlog[Debug::CRIT] << "(GUI::main): " << ex.what() << endl;
	}
	catch (...)
	{
		dlog[Debug::CRIT] << "(GUI::main): catch ..." << endl;
	}

	return 1;
}
