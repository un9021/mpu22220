#!/bin/sh

dbname=archive.db

[ -n "$1" ] && dbname="$1"

#  date date NOT NULL,
#  time time NOT NULL,
sqlite3 $dbname <<"_EOF_"

DROP TABLE IF EXISTS journal;
CREATE TABLE journal (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  tms timestamp KEY default (strftime('%s', 'now')),
  aid TEXT NOT NULL,
  type INTEGER NOT NULL,
  text TEXT NOT NULL,
  bg TEXT NOT NULL,
  numcon TEXT NOT NULL
);

DROP TABLE IF EXISTS sensors;
CREATE TABLE sensors (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  tms timestamp KEY default (strftime('%s', 'now')),
  msec INTEGER default 0,
  sid INTEGER NOT NULL,
  val DOUBLE NOT NULL
);

_EOF_
