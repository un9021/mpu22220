#include "Setting.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace MPU;
using namespace uniset;
// -------------------------------------------------------------------------
shared_ptr<Setting> Setting::create( const string& name, const string& prefix )
{
	auto conf = uniset::uniset_conf();
	uniset::ObjectId id = conf->getObjectID(name);

	if( id == uniset::DefaultObjectId )
		throw uniset::SystemError("(make_object<" + string(typeid(Setting).name()) + ">): Not found ID for '" + name + "'");

	auto xml = conf->getConfXML();
	xmlNode* cnode = xml->findNode(xml->getFirstNode(), "Setting", name);

	if( cnode == 0 )
		throw uniset::SystemError("(make_object<" + string(typeid(Setting).name()) + ">): Not found xmlnode <" + name + " name=\"" + name + "\" ... >");

	std::shared_ptr<Setting> set = std::make_shared<Setting>(id, cnode, prefix);

	if (set == nullptr)
		throw uniset::SystemError("(make_object<T>  == nullptr" + string(typeid(Setting).name()));

	return set;
}
// -------------------------------------------------------------------------
Setting::Setting( uniset::ObjectId id, xmlNode* cnode, const std::string& prefix ):
	enMenu(0),
	mainMenu(0),
	btnSetup(0),
	btnSetupExit(0)
{
	dlgPass = PasswordDialog::Instance();
	GETWIDGET(mainMenu, "mainmenu");
	GETWIDGET(enMenu, "enmenu");
	GETWIDGET(journal, "Journal");
	GETWIDGET(notebookjrn, "notebookjrn"); //Журнал
	//Обработчик открытия меню Инженерного меню
	GETWIDGET(btnSetup, "ctl_btn_Setup");
	btnSetup->signal_clicked().connect(sigc::mem_fun(*this, &Setting::btnSetup_clicked));
	//Обработчик закрытия меню Инженерного меню
	GETWIDGET(btnSetupExit, "ctl_btn_SetupExit");
	btnSetupExit->signal_clicked().connect(sigc::mem_fun(*this, &Setting::btnSetupExit_clicked));
}
// -------------------------------------------------------------------------
Setting::~Setting()
{
}
// -------------------------------------------------------------------------
void Setting::btnSetup_clicked()
{
	if(dlgPass->check("Engineer"))
	{
		notebookjrn->hide();
		mainMenu->hide();
		enMenu->show();
	}
}
// -------------------------------------------------------------------------
void Setting::btnSetupExit_clicked()
{
	enMenu->hide();
	mainMenu->show();
}
// -------------------------------------------------------------------------
