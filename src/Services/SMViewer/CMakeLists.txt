configure_file(${CMAKE_CURRENT_SOURCE_DIR}/ctl-smviewer-${PACKAGE}.sh.in ${CMAKE_CURRENT_SOURCE_DIR}/ctl-smviewer-${PACKAGE}.sh)

add_custom_target(smviewer_links ln -s -f /usr/bin/uniset2-stop.sh stop.sh && ln -f -s ../../../conf/configure.xml)

install(FILES ctl-smviewer-${PACKAGE}.sh DESTINATION /usr/bin/)