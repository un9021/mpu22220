FROM un9021/altlinux-p7-32bit-jeos
MAINTAINER un
RUN apt-get update -y
RUN apt-get upgrade -y
#готовим среду для работы(для того, чтобы операции кэшировались, каждую утилиту отдельной командой)
RUN apt-get install -y mc 
RUN apt-get install -y git gitk git-gui
RUN apt-get install -y etersoft-build-utils
RUN apt-get install -y automake_1.11
RUN apt-get install -y autoconf_2.60
RUN apt-get install -y libxml++2-devel
RUN apt-get install -y libomniORB-devel
RUN apt-get install -y libcommoncpp2-devel
RUN apt-get install -y libsqlite3-devel
RUN apt-get install -y librrd-devel
RUN apt-get install -y libcomedi-devel
RUN apt-get install -y xsltproc
RUN apt-get install -y doxygen
RUN apt-get install -y convert
RUN apt-get install -y ccache
RUN apt-get install -y xvfb-run
RUN apt-get install -y openssh openssh-server openssh-common openssh-clients openssh-keysign openssh-server-control
RUN apt-get install -y passwd
RUN apt-get install -y xorg-server
RUN apt-get install -y x11vnc
RUN apt-get install -y openbox
RUN apt-get install -y libev-devel --fix-missing
RUN apt-get install -y MySQL-client --fix-missing
RUN apt-get install -y libpqxx-devel --fix-missing
RUN apt-get install -y python-module-distribute --fix-missing
RUN apt-get install -y graphviz --fix-missing
RUN apt-get install -y cmake
RUN apt-get install -y libglade-devel
RUN apt-get install -y libglademm-devel
RUN apt-get install -y libcairo-devel
RUN apt-get install -y libgtest-devel
RUN apt-get install -y libmosquitto-devel
RUN apt-get install -y libgladeui2.0-devel --fix-missing
RUN apt-get install -y astyle --fix-missing
RUN apt-get install -y librsvg-devel --fix-missing
RUN apt-get install -y libXtst-devel --fix-missing
RUN apt-get install -y catch --fix-missing
RUN apt-get install -y su --fix-missing
RUN apt-get install -y python-module-libxml2 --fix-missing
RUN apt-get install -y python-module-lxml --fix-missing
RUN apt-get install -y monit --fix-missing
RUN apt-get install -y libomniORB-names --fix-missing
RUN apt-get install -y systemd --fix-missing
RUN apt-get install -y vsftpd --fix-missing
RUN apt-get install -y anonftp --fix-missing
RUN apt-get install -y libmysqlclient-devel --fix-missing
RUN apt-get install -y i3 i3status --fix-missing
RUN apt-get install -y vim-console --fix-missing
RUN apt-get install -y python-module-pygobject-devel --fix-missing
RUN apt-get install -y net-snmp-clients --fix-missing
RUN apt-get install -y apt-repo-tools --fix-missing
RUN apt-get install -y rdate --fix-missing
RUN apt-get install -y libwebkitgtk2-devel --fix-missing
RUN apt-get install -y libgladeui-devel --fix-missing
RUN apt-get install -y glade3 --fix-missing
RUN apt-get install -y libiodbc --fix-missing
RUN apt-get install -y libunixODBC-devel --fix-missing
RUN apt-get install -y python-modules-json --fix-missing
RUN useradd -u 1000 user
WORKDIR /home/user/
COPY ./passwd /home/user/.vnc/
COPY ./vnc.sh /home/user/
COPY ./build.sh /home/user
COPY ./vsftpd /etc/xinetd.d/vsftpd
COPY ./apt.conf /etc/apt
COPY ./install.sh /home/user/
COPY ./genb.sh /var/ftp/pub/Ourside/
COPY ./RPMp7/i586/ /var/ftp/pub/Ourside/i586/
RUN chown user:user /var/ftp/pub/*
RUN chown user:user /var/ftp/pub/Ourside/*
RUN chown user:user /var/ftp/pub/Ourside/i586/*
RUN chown user:user /var/ftp/pub/Ourside/i586/RPMS.uniset2/*
RUN chown user:user /var/ftp/pub/Ourside/i586/RPMS.mpulib3/*
RUN chown user:user /var/ftp/pub/Ourside/i586/RPMS.mpu22220/*
RUN [ "chmod","777","/home/user/vnc.sh" ]
RUN [ "chmod","777","/home/user/build.sh" ]
RUN service sshd stop
RUN service sshd start
RUN usermod -a -G wheel user
RUN echo "user ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/user
RUN [ "chmod","400","/etc/sudoers.d/user" ]
RUN mkdir -p /var/ftp/pub/Ourside/i586
USER user
RUN git config --global user.email "user@example.com"
RUN git config --global user.name "user"
ENTRYPOINT ./vnc.sh && ./install.sh; /bin/bash
