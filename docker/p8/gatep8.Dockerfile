FROM un9021/altlinux-p8-32bit-jeos
MAINTAINER un
RUN apt-get update -y
RUN apt-get upgrade -y
#готовим среду для работы(для того, чтобы операции кэшировались, каждую утилиту отдельной командой)
RUN apt-get install -y mc 
RUN apt-get install -y git gitk git-gui
RUN apt-get install -y etersoft-build-utils
RUN apt-get install -y automake_1.14
RUN apt-get install -y autoconf_2.60
RUN apt-get install -y libxml++2-devel
RUN apt-get install -y libomniORB-devel
RUN apt-get install -y libcommoncpp2-devel
RUN apt-get install -y libsqlite3-devel
RUN apt-get install -y librrd-devel
RUN apt-get install -y libcomedi-devel
RUN apt-get install -y xsltproc
RUN apt-get install -y doxygen
RUN apt-get install -y convert
RUN apt-get install -y ccache
RUN apt-get install -y xvfb-run
RUN apt-get install -y x11vnc-service
RUN apt-get install -y openssh openssh-server openssh-common openssh-clients openssh-keysign openssh-server-control
RUN apt-get install -y passwd
RUN apt-get install -y xorg-server
RUN apt-get install -y x11vnc x11vnc-service
RUN apt-get install -y openbox
RUN apt-get install -y libev-devel --fix-missing
RUN apt-get install -y libpoco-devel --fix-missing
RUN apt-get install -y MySQL-client --fix-missing
RUN apt-get install -y libpqxx-devel --fix-missing
RUN apt-get install -y python-module-distribute --fix-missing
RUN apt-get install -y graphviz --fix-missing
RUN apt-get install -y cmake
RUN apt-get install -y libglade-devel
RUN apt-get install -y libglademm-devel
RUN apt-get install -y libcairo-devel
RUN apt-get install -y libgtest-devel
RUN apt-get install -y libmosquitto-devel
RUN apt-get install -y libgladeui2.0-devel --fix-missing
RUN apt-get install -y astyle --fix-missing
RUN apt-get install -y librsvg-devel --fix-missing
RUN apt-get install -y libXtst-devel --fix-missing
RUN apt-get install -y catch --fix-missing
RUN apt-get install -y su --fix-missing
RUN apt-get install -y python-module-libxml2 --fix-missing
RUN apt-get install -y python-module-lxml --fix-missing
RUN apt-get install -y monit --fix-missing
RUN apt-get install -y libomniORB-names --fix-missing
RUN apt-get install -y systemd --fix-missing
RUN apt-get install -y vsftpd --fix-missing
RUN apt-get install -y anonftp --fix-missing
RUN apt-get install -y libmysqlclient20-devel --fix-missing
RUN apt-get install -y i3 i3status --fix-missing
RUN apt-get install -y vim-console --fix-missing
RUN apt-get install -y python-module-pygobject-devel --fix-missing
RUN apt-get install -y net-snmp-clients --fix-missing
RUN apt-get install -y rdate --fix-missing
COPY ./install_gate.sh /root/
COPY ./hosts /root/
RUN touch /dev/ttyS2
RUN touch /dev/ttyS3
RUN echo "" > /etc/apt/sources.list.d/alt.list
RUN echo "rpm ftp://192.168.177.100/pub/Ourside i586 uniset2 mpulib3 mpu22220" > /etc/apt/sources.list.d/mpu22220.list
ENTRYPOINT ./vnc.sh; /bin/bash
