#!/bin/sh

RETVAL=0
PROG="${0##*/}"
ACTION="ping"
LIST="mpu22220.gate11_1 mpu22220.gate12_1 mpu22220.gate21_1 mpu22220.gate22_1 mpu22220.gate31_1 mpu22220.gate32_1 mpu22220.gui11_1 mpu22220.gui12_1 mpu22220.gui21_1 mpu22220.gui22_1 mpu22220.gui31_1 mpu22220.gui32_1
mpu22220.gate11_2 mpu22220.gate12_2 mpu22220.gate21_2 mpu22220.gate22_2 mpu22220.gate31_2 mpu22220.gate32_2 mpu22220.gui11_2 mpu22220.gui12_2 mpu22220.gui21_2 mpu22220.gui22_2 mpu22220.gui31_2 mpu22220.gui32_2"
PID=
for HOST in $LIST; do
	if ping -c 3 -i 0.3 -w 1 "$HOST" 1>>/dev/null 2>>/dev/null; then
		echo "$HOST ping OK"
	else
		echo "$HOST ping FAILED"
	fi&
done
wait
exit $RETVAL
