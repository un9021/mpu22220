#!/bin/sh
RETVAL=0
PROG="${0##*/}"
NODES="mpu22220.gui11_1
mpu22220.gui12_1
mpu22220.gui21_1
mpu22220.gui22_1
mpu22220.gui31_1
mpu22220.gui32_1
mpu22220.gate11_1
mpu22220.gate12_1
mpu22220.gate21_1
mpu22220.gate22_1
mpu22220.gate31_1
mpu22220.gate32_1
"
LOGDIR=$HOME/logs
DEBUG=
SSHUSER=root
CMD="$1"

print_usage()
{
    [ "$1" = 0 ] || exec >&2
	cat <<EOF

Usage: $PROG [ start|stop|restart|reboot|poweroff ] [ $NODES | all ]

EOF
    [ -n "$1" ] && exit "$1" || exit
}

LIST=


if [ "$2" == "all" ]; then
	LIST=$NODES 
else
	for p in `seq 2 14`; do
		eval t=\$$p
		[ -z "$t" ] && break
		LIST="$LIST $t" 
	done

fi

[ -z "$LIST" ] && print_usage 1
[ -z "$CMD" ] && print_usage 1

KSSH=
[ -z "$DEBUG" ] && KSSH="-T" || KSSH=

PID=

HEAD=`date "+%Y-%m-%d %H:%M"`

mkdir -p $LOGDIR

# install
for n in $LIST; do
	
	echo "'$n' run command '$CMD'..."

	LOG="$LOGDIR/$n.command.log"

	echo "                     ">>$LOG
	echo "                     ">>$LOG
	echo "=====================">>$LOG
	echo $HEAD>>$LOG
	echo "=====================">>$LOG
	if ssh -o StrictHostKeychecking=no $KSSH -x $SSHUSER@$n /usr/bin/ctl-system.sh $CMD 1>>$LOG 2>>$LOG; then
		echo "'$n' $CMD OK">>$LOG 
		echo "'$n' $CMD OK"
	else
		echo "'$n' $CMD FAILED. See log '$LOG' for details..."
		echo "'$n' $CMD FAILED. ">>$LOG
	fi &

	PID=$!
done

wait

exit $RETVAL
