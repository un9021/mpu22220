#!/bin/bash
. make_common.sh
[ -z "$correctlist" ] && correctlist=common_rules
input=$1
number=$2
shift_number=800
[ -z "$number" ] && number=11000
cur_number=${number};
PROG="${0##*/}"

print_help()
{
      [ "$1" = 0 ] || exec >&2
      cat <<EOF
      Usage: $PROG name_file [BEGIN_ID]
      name_file - input file(*.csv with separator ";")
      BEGIN_ID - first id item. Default: $number
EOF
    [ -n "$1" ] && exit "$1" || exit
}

[[ -z "$1" ]] && print_help 1
numb_gen=1
rules=$(get_autozamena_sed_rules "$correctlist")
extra_rules=
for GED in "GED1" "GED2" "GED3"; do
	gedl=$(echo $GED | tr '[:upper:]' '[:lower:]')
	echo "			<!-- $GED -->"
	
	if [[ $GED == "GED1" ]]; 
	then 
		extra_rules="s:gedX:ged1:g;s:GEDX:GED1:g;s:ГЭДX:ГЭД1:g;s:TVX:TV1:g;s:Тр-рX:Тр-р1:g;s:FCX:FC1:g;s:ПЧ X:ПЧ 1:g;s:winTransX:winTrans1:g;s:winDriveX:winDrive1:g;s:winDriveTempX:winDriveTemp1:g;s:drivewinTempX1:drivewinTemp11:g;s:drivewinTempX2:drivewinTemp12:g;s:transTempX1:transTemp11:g;s:transTempX2:transTemp12:g;s:transTempX3:transTemp13:g;s:transTempX4:transTemp14:g"
	fi
	if [[ $GED == "GED2" ]]; 
	then 
		extra_rules="s:gedX:ged2:g;s:GEDX:GED2:g;s:ГЭДX:ГЭД2:g;s:TVX:TV2:g;s:Тр-рX:Тр-р2:g;s:FCX:FC2:g;s:ПЧ X:ПЧ 2:g;s:winTransX:winTrans2:g;s:winDriveX:winDrive2:g;s:winDriveTempX:winDriveTemp2:g;s:drivewinTempX1:drivewinTemp21:g;s:drivewinTempX2:drivewinTemp22:g;s:transTempX1:transTemp21:g;s:transTempX2:transTemp22:g;s:transTempX3:transTemp23:g;s:transTempX4:transTemp24:g"
	fi
	if [[ $GED == "GED3" ]]; 
	then 
		extra_rules="s:gedX:ged3:g;s:GEDX:GED3:g;s:ГЭДX:ГЭД3:g;s:TVX:TV3:g;s:Тр-рX:Тр-р3:g;s:FCX:FC3:g;s:ПЧ X:ПЧ 3:g;s:winTransX:winTrans3:g;s:winDriveX:winDrive3:g;s:winDriveTempX:winDriveTemp3:g;s:drivewinTempX1:drivewinTemp31:g;s:drivewinTempX2:drivewinTemp32:g;s:transTempX1:transTemp31:g;s:transTempX2:transTemp32:g;s:transTempX3:transTemp33:g;s:transTempX4:transTemp34:g"
	fi
	
	while read line; do
		echo $line | sed "s:id;:		<item id=\"$cur_number:g;
		${rules};$drive_rules;${extra_rules}"
		cur_number=$((cur_number+1))
	done < $input
	
	cur_number=$((number+shift_number))
	number=${cur_number};
	numb_gen=$((numb_gen+1))
done
