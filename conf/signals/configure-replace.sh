#!/bin/bash

PROG="${0##*/}"
SRC_FILE=$1
REPL_FILE=$2

BEGIN_TAG=$3
END_TAG=$4

ECHO_OK=1
OLD_IFS=$IFS

print_help()
{
	  [ "$1" = 0 ] || exec >&2
	  cat <<EOF
Usage:	$PROG SRC_FILE REPL_FILE BEGIN_TAG END_TAG

	SRC_FILE		- файл, в который вставляем измененные датчики (configure.xml)
	REPL_FILE		- файл со списком датчиков после выполнения скрипта *_make.sh
	BEGIN_TAG		- начальный тег, с которого начинается область замены в SRC_FILE
	END_TAG		- конец области замены в SRC_FILE
EOF
	[ -n "$1" ] && exit "$1" || exit
}

[ -z "$1" ] && print_help 1

IFS=""; while read line; do

 if echo $line | grep -q "<!--.*$BEGIN_TAG*.-->"; then
 echo $line
 ECHO_OK= # отключаем вывод
 continue
 fi

 if echo $line | grep -q "<!--.*$END_TAG*.-->"; then
 # проверим а был ли BEGIN (если да то, вставляем замену)
 [ -z "$ECHO_OK" ] && cat ${REPL_FILE} && ECHO_OK=1
 echo $line && continue
 fi

 [ -n "$ECHO_OK" ] && echo "$line"

done < ${SRC_FILE}

IFS=$OLD_IFS
