#!/bin/bash

#Скрипт-болванка для создания ..._make.sh

. "./make_common.sh"

[ -z "$correctlist" ] && correctlist=common_rules

input=$1
#Номер, с которого начнется отсчет id
number=$2
[ -z "$number" ] && number=1
#Сдвиговый параметр, на сколько будет отступ между датчиками (например, между TV1 и TV2)
inc_number=5000

shift_number=$number

PROG="${0##*/}"

print_help()
{
		[ "$1" = 0 ] || exec >&2
		cat <<EOF
		Usage: $PROG name_file [BEGIN_ID]
		name_file - input file(*.csv with separator ";")
		BEGIN_ID - first id item. Default: $number
EOF
    [ -n "$1" ] && exit "$1" || exit
}

[[ -z "$1" ]] && print_help 1

#Генерируем правила замен и common_rules
rules=$(get_autozamena_sed_rules "$correctlist")
need="w\;1"

gen_w()
{
        if  echo $line | grep -q $need 
        then
        number1=$((number+1000))
        number2=$((number+2000))
	number3=$((number+3000))
	number4=$((number+4000))

        
        if echo $line | grep -q "forall\;1"
	then
		#Генерация датчиков для записи в ПЧ
        echo $line | sed "s|id;|		<item id=\"$number|g;
			s|<!--|		<!--|g;
			s|;mbgate_mbtype;rtu||g;
			s|;mbgate_mbaddr;0x01||g;
			s|;mbgate_mbfunc;0x04||g;
			s|;mbgate_mbreg;||g;
			s|;tcpgate;MPU||g;
			s|;mbgui_mbreg;||g;
			s|;mbgui_mbaddr;0x01||g;
			s|;mbgui_mbfunc;0x04||g;
			s|;mbgui_mbtype;rtu||g;
			s|;mbsvu_mbaddr;0x01||g;
			s|;mbsvu_mbfunc;0x04||g;
			s|;mbsvu_mbtype;rtu||g;
			s|;mbsvu_mbreg;||g;
			s|;tcpgui;MPU||g;
			s|;tcpsvu;MPU||g;
			s|AI.*direct|AI;direct|g;
			s|MPU|$OBJ|g;
			s|from|to|g;
			s|0x04|0x10|g;${OBJ_RULSE}${rules}${OBJ_RULSE2}"
		#Генерация датчиков для записи из панельника
        echo $line | sed "s|id;|		<item id=\"$number1|g;
			s|<!--|		<!--|g;
			s|;mbsvu_mbaddr;0x01||g;
			s|;mbsvu_mbfunc;0x04||g;
			s|;mbsvu_mbtype;rtu;||g;
			s|;tcpgui;|\" tcpgui=\"|g;
			s|;tcpsvu;MPU||g;
			s|;mbgui_mbreg;|\" mbgui_mbreg=\"$number|g;
			s|;mbgui_mbfunc;0x04|;mbgui_mbfunc;0x10|g;
			s|;mbsvu_mbreg;||g;
			s|name\;[^\;]*|\02|;
			s|MPU|$OBJ|g;
			s|direct;from||g;
			s|direct;to||g;
			s|;mbgate_mbreg;||g;
			s|;mbgate_mbaddr;0x01||g;
			s|;mbgate_mbfunc;0x04||g;
			s|;mbgate_mbtype;rtu||g;
			s|;tcpgate;$OBJ||g;${OBJ_RULSE}${rules}${OBJ_RULSE2}"
		#Генерация датчиков для записи из СВУ	
        echo $line | sed "s|id;|		<item id=\"$number2|g;
			s|<!--|		<!--|g;
			s|;tcpgui;MPU||g;
			s|;tcpsvu;|\" tcpsvu=\"|g;
			s|tcpgui=\"MPU1;||g;
			s|;mbsvu_mbreg;|\" mbsvu_mbreg=\"$number|g;
			s|mbgate_.*tcpgate;GATE;||g;
			s|name\;[^\;]*|\03|;
			s|MPU|$OBJ|g;
			s|direct;from||g;
			s|direct;to||g;
			s|;mbgate_mbreg;||g;
			s|;mbgate_mbaddr;0x01||g;
			s|;mbgate_mbfunc;0x04||g;
			s|;mbgate_mbtype;rtu||g;
			s|;tcpgate;$OBJ||g;
			s|;mbgui_mbreg;||g;
			s|;mbgui_mbaddr;0x01||g;
			s|;mbgui_mbfunc;0x04||g;
			s|;mbgui_mbtype;rtu||g;
			s|;tcpgui;$OBJ||g;${OBJ_RULSE}${rules}${OBJ_RULSE2}"
		#Генерация датчиков для записи с соседнего ПЧ
         echo $line | sed "s|id;|		<item id=\"$number3|g;
			s|<!--|		<!--|g;
			s|;tcpgui;MPU||g;
			s|tcpgui=\"MPU1;||g;
			s|;mbsvu_mbreg;|\" mbsvu_mbreg=\"$number3|g;
			s|mbgate_.*tcpgate;GATE;||g;
			s|name\;[^\;]*|\04|;
			s|tcpgate;MPU||g;
			s|direct;from||g;
			s|direct;to||g;
			s|;mbgate_mbreg;||g;
			s|;mbgate_mbaddr;0x01||g;
			s|;mbgate_mbtype;rtu||g;
			s|;mbgate_mbfunc;0x04||g;
			s|;mbgui_mbreg;||g;
			s|;mbgui_mbaddr;0x01||g;
			s|;mbgui_mbfunc;0x04||g;
			s|;mbgui_mbtype;rtu||g;
			s|;tcpsvu;|\" tcpsvu=\"|g;
			s|MPU|$OBJ|g;
			${OBJ_RULSE}${rules}${OBJ_RULSE2}"
         echo $line | sed "s|id;|		<item id=\"$number4|g;
			s|<!--|		<!--|g;
			s|;tcpgui;MPU||g;
			s|tcpgui=\"MPU1;||g;
			s|;mbsvu_mbreg;||g;
			s|;mbsvu_mbaddr;0x01||g;
			s|;mbsvu_mbtype;rtu||g;
			s|;mbsvu_mbfunc;0x04||g;
			s|mbgate_.*tcpgate;GATE;||g;
			s|name\;[^\;]*|\05|;
			s|tcpgate;MPU|tcpgate;GATE|g;
			s|;mbgate_mbfunc;0x04|\" mbgate_mbfunc=\"0x10|;
			s|direct;from||g;
			s|direct;to||g;
			s|;mbgate_mbreg;|\" mbgate_mbreg=\"$number3|g;
			s|;mbgui_mbreg;||g;
			s|;mbgui_mbaddr;0x01||g;
			s|;mbgui_mbfunc;0x04||g;
			s|;mbgui_mbtype;rtu||g;
			s|;tcpsvu;MPU||g;
			s|MPU|$OBJ|g;
			${OBJ_RULSE}${rules}${OBJ_RULSE2}"

		else
        echo $line | sed "s|id;|		<item id=\"$number|g;s|<!--|		<!--|g;s|;tcpgui;|\" tcpgui=\"|g;s|;tcpsvu;|\" tcpsvu=\"|g;s|;mbgui_mbreg;|\" mbgui_mbreg=\"$number|g;s|;mbsvu_mbreg;|\" mbsvu_mbreg=\"$number|g;s|MPU|$OBJ|g;s|0x04|0x10|g;s|from|to|g;${OBJ_RULSE}${rules}${OBJ_RULSE2}"
        fi
        
        fi
}

gen()
{
if [[ "$OBJ" == "MPU2" ]]; then
    number3=$((number-10000))
else
number3=$number
fi
        echo $line | sed "s|id;|		<item id=\"$number|g;s|<!--|		<!--|g;s|;tcpgui;|\" tcpgui=\"|g;s|;tcpsvu;|\" tcpsvu=\"|g;s|;mbgui_mbreg;|\" mbgui_mbreg=\"$number3|g;s|;mbsvu_mbreg;|\" mbsvu_mbreg=\"$number3|g;s|;mbgate_mbreg;|\" mbgate_mbreg=\"$number3|g;s|MPU|$OBJ|g;${OBJ_RULSE}${rules}${OBJ_RULSE2}"
}

for OBJ in "MPU1" "w_MPU1" "MPU2" "w_MPU2"; do

	echo "		<!-- Датчики "$OBJ" (сгенерировано из $1 csv) $USER: `date -u +"%d.%m.%Y"` -->"

	#Иногда требуется то же название, но прописными буквами
	OBJl=$(echo $OBJ | tr '[:upper:]' '[:lower:]')

    case "$OBJ" in
    "MPU1" )
        OBJ_RULSE="s|fqcTemp|fqcTemp1|g;s|lsuTemp|lsuTemp1|g;s|driveTemp1|drivewinTemp1|g;s|driveTemp2|drivebeaTemp3|g;s|tTemp1|transTemp1|g;s|tTemp2|transTemp3|g;s|tcpgate=\"w_MPU1|tcpgate=\"MPU1|g"
        OBJ_RULSE2=";s|DDD|1|g;"
        ;;
    "MPU2" )
        OBJ_RULSE="s|fqcTemp|fqcTemp2|g;s|lsuTemp|lsuTemp2|g;s|driveTemp1|drivewinTemp2|g;s|driveTemp2|drivebeaTemp4|g;s|tTemp1|transTemp2|g;s|tTemp2|transTemp4|g"
        OBJ_RULSE2=";s|DDD|2|g;"
        ;;
    esac

	#Цикл чтения из csv
	while read line; do
		
    case "$OBJ" in
    "MPU1" )
            gen
        ;;
    "w_MPU1" )
            gen_w
        ;;
    "MPU2" )
            gen
        ;;
    "w_MPU2" )
            gen_w
        ;;
    esac
		#Увеличение id
		number=$((number+1))
	done < $input

	let number=$((shift_number+inc_number))
	shift_number=$number

done
