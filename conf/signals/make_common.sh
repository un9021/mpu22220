#!/bin/sh

#Скрипт с общими функциями

get_autozamena_sed_rules()
{
	OLD_IFS=$IFS
	[[ -z "$1" ]] && echo "(get_autozamena_sed_rules): ERROR: Unknown file rules..." && exit 1
	list=$1
	sed_rules=
	while read line; do
	IFS="		"
	set -- $line
	sed_rules="${sed_rules};s:$1:$2:g"
	done < $list
	IFS=$OLD_IFS
	echo $sed_rules
}
