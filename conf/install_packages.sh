#!/bin/sh
RETVAL=0
PROG="${0##*/}"
NODES="mpu22220.gui11_1
mpu22220.gui12_1
mpu22220.gui21_1
mpu22220.gui22_1
mpu22220.gui31_1
mpu22220.gui32_1
mpu22220.gate11_1
mpu22220.gate12_1
mpu22220.gate21_1
mpu22220.gate22_1
mpu22220.gate31_1
mpu22220.gate32_1
"
LOGDIR=$HOME/logs
DEBUG=
SSHUSER=root

print_usage()
{
    [ "$1" = 0 ] || exec >&2
	cat <<EOF

Usage: $PROG [ $NODES ] | all

EOF
    [ -n "$1" ] && exit "$1" || exit
}

LIST=

[ "$1" == "all" ] && LIST=$NODES || LIST="$*"

[ -z "$LIST" ] && print_usage 1

KSSH=
[ -z "$DEBUG" ] && KSSH="-T" || KSSH=

PID=

HEAD=`date "+%Y-%m-%d %H:%M"`

mkdir -p $LOGDIR

# install
for n in $LIST; do
	echo "'$n' starting update..."

	LOG="$LOGDIR/$n.update.log"

	echo "                     ">>$LOG
	echo "                     ">>$LOG
	echo "=====================">>$LOG
	echo $HEAD>>$LOG
	echo "=====================">>$LOG
	if ssh -o StrictHostKeychecking=no $KSSH -x $SSHUSER@$n /usr/bin/ctl-update.sh mpu22220 1>>$LOG 2>>$LOG; then
		echo "'$n' update OK">>$LOG 
		echo "'$n' update OK"
	else
		echo "'$n' update FAILED. See log '$LOG' for details..."
		echo "'$n' update FAILED. ">>$LOG
	fi &

	PID=$!
done

#wating
wait

exit $RETVAL
