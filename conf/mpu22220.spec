Name: mpu22220
Version: 0.3
Release: set42.60
Summary: MPU 22220

License: GPL
Group: Development/C++
Url: http://setri.ru

Source: %name-%version.tar

Requires: mpulib3 >= 3.1-set10 libuniset2 >= 2.7-alt5

# Automatically added by buildreq on Wed Aug 25 2010
BuildRequires: libXtst-devel libglademm-devel libuniset2-devel python-modules mpulib3-devel

%description
MPU. This is special internal project for SET Research Instinute

%package gate
Summary: Interface converter for MPU
Group: Development/Other
Requires: %name = %version-%release
Requires: libuniset2-extension-sqlite
Requires: mpu22220
Requires: mpulib3-gate
Requires: mpu3-gate

%description gate
Converter station for MPU project

%package gate-fc11
Summary: Interface converter for MPU
Group: Development/Other
Requires: %name = %version-%release
Requires: mpu22220-gate

%description gate-fc11
Converter station for MPU project

%package gate-fc12
Summary: Interface converter for MPU
Group: Development/Other
Requires: %name = %version-%release
Requires: mpu22220-gate

%description gate-fc12
Converter station for MPU project

%package gate-fc21
Summary: Interface converter for MPU
Group: Development/Other
Requires: %name = %version-%release
Requires: mpu22220-gate

%description gate-fc21
Converter station for MPU project

%package gate-fc22
Summary: Interface converter for MPU
Group: Development/Other
Requires: %name = %version-%release
Requires: mpu22220-gate

%description gate-fc22
Converter station for MPU project

%package gate-fc31
Summary: Interface converter for MPU
Group: Development/Other
Requires: %name = %version-%release
Requires: mpu22220-gate

%description gate-fc31
Converter station for MPU project

%package gate-fc32
Summary: Interface converter for MPU
Group: Development/Other
Requires: %name = %version-%release
Requires: mpu22220-gate

%description gate-fc32
Converter station for MPU project

%package gui
Summary: Common package for MPU
Group: Development/Other
Requires: %name = %version-%release
Requires: autologin
Requires: xinit
Requires: mpu22220
Requires: mpulib3-gui
Requires: mpu3-gui

%description gui
GUI for for MPU

%package gui-fc11
Summary: Common package for MPU
Group: Development/Other
Requires: %name = %version-%release
Requires: mpu22220-gui

%description gui-fc11
GUI1 for for MPU

%package gui-fc12
Summary: Common package for MPU
Group: Development/Other
Requires: %name = %version-%release
Requires: mpu22220-gui

%description gui-fc12
GUI2 for for MPU

%package gui-fc21
Summary: Common package for MPU
Group: Development/Other
Requires: %name = %version-%release
Requires: mpu22220-gui

%description gui-fc21
GUI1 for for MPU

%package gui-fc22
Summary: Common package for MPU
Group: Development/Other
Requires: %name = %version-%release
Requires: mpu22220-gui

%description gui-fc22
GUI2 for for MPU

%package gui-fc31
Summary: Common package for MPU
Group: Development/Other
Requires: %name = %version-%release
Requires: mpu22220-gui

%description gui-fc31
GUI1 for for MPU

%package gui-fc32
Summary: Common package for MPU
Group: Development/Other
Requires: %name = %version-%release
Requires: mpu22220-gui

%description gui-fc32
GUI2 for for MPU

%package doc
Summary: Documentation for MPU
Group: Development/Other
Requires: %name = %version-%release

%description doc
Documentation for MPU Project

%prep
%setup

%build
%autoreconf
%if_enabled doc
	%configure --with-doxygen
%else
	%configure
%endif

%make_build
%make

%install
%makeinstall
mkdir -p -m755 %buildroot/var/local/log/%name
mkdir -p -m755 %buildroot/var/local/run/%name
mkdir -p -m755 %buildroot/var/local/lock/%name/{system,alarmdb}
mkdir -p -m755 %buildroot/var/local/cache/%name/{system,alarmdb}
mkdir -p -m755 %buildroot/var/local/omniORB
mkdir -p -m644 %buildroot%_datadir/%name
mkdir -p -m755 %buildroot/var/local/%name/db
mkdir -p -m755 %buildroot/var/local/%name/backup

mkdir -p -m755 %buildroot%_initdir

mkdir -p -m755 %buildroot%_datadir/mpu3
echo "%name-%version-%release" > %buildroot%_datadir/mpu3/VERSION.txt
echo "%name-%version-%release" > %buildroot%_datadir/%name/VERSION.txt

subst "s|img=\"|img=\"/usr/share/mpu22220/images/|g;s|dlgSetVal=\"mpu22220-gui.ui\"|dlgSetVal=\"/usr/share/mpu22220/mpu22220-gui.ui\"|g" %buildroot/etc/%name/guiconfigure.xml
subst "s|images/images|images|g" %buildroot/etc/%name/guiconfigure.xml




NLIST="fc11 fc12 fc21 fc22 fc31 fc32"
for n in ${NLIST};
do
	cp -f %buildroot/etc/%name/guiconfigure.xml %buildroot%_sysconfdir/%name/guiconfigure.xml.$n
	case "$n" in
	"fc11" )
	rules="s:winTransX1:winTrans11:g;s:winTransX2:winTrans12:g;s:winTransX3:winTrans13:g;s:winTransX4:winTrans14:g"
	;;
	"fc12" )
	rules="s:winTransX1:winTrans12:g;s:winTransX2:winTrans11:g;s:winTransX3:winTrans14:g;s:winTransX4:winTrans13:g"
	;;
	"fc21" )
	rules="s:winTransX1:winTrans21:g;s:winTransX2:winTrans22:g;s:winTransX3:winTrans23:g;s:winTransX4:winTrans24:g;s:MT_ConfirmGED1_S:MT_ConfirmGED2_S:g;s:MT_TargetGED1_S:MT_TargetGED2_S:g"
	;;
	"fc22" )
	rules="s:winTransX1:winTrans22:g;s:winTransX2:winTrans21:g;s:winTransX3:winTrans24:g;s:winTransX4:winTrans23:g;s:MT_ConfirmGED1_S:MT_ConfirmGED2_S:g;s:MT_TargetGED1_S:MT_TargetGED2_S:g"
	;;
	"fc31" )
	rules="s:winTransX1:winTrans31:g;s:winTransX2:winTrans32:g;s:winTransX3:winTrans33:g;s:winTransX4:winTrans34:g;s:MT_ConfirmGED1_S:MT_ConfirmGED3_S:g;s:MT_TargetGED1_S:MT_TargetGED3_S:g"
	;;
	"fc32" )
	rules="s:winTransX1:winTrans32:g;s:winTransX2:winTrans31:g;s:winTransX3:winTrans34:g;s:winTransX4:winTrans33:g;s:MT_ConfirmGED1_S:MT_ConfirmGED3_S:g;s:MT_TargetGED1_S:MT_TargetGED3_S:g"
	;;
	esac

	subst "$rules" %buildroot%_sysconfdir/%name/guiconfigure.xml.$n
done




for n in ${NLIST};
do
	cp -f %buildroot%_sysconfdir/%name/configure.xml %buildroot%_sysconfdir/%name/configure.xml.$n
	case "$n" in
	"fc11" )
	rules="s|\"NumFQC\"|\"11\"|g;s|winTrans1TV1|winTrans1|g;s|winTrans2TV1|winTrans3|g;s|winTrans3TV1|winTrans2|g;s|winTrans4TV1|winTrans4|g;s|gatembn1|gate112|g;s|gatembn2|gate212|g;s|gaten1|gate111|g;s|gaten2|gate211|g;s|gaten3|gate112|g;s|gaten4|gate212|g;s|MPU1_CState_AS|FQC11_CState_AS|g;s|MPU2_CState_AS|FQC12_CState_AS|g;s|ged_plc_1|ged1_io_1|g;s|ged_plc_2|ged1_io_2|g;s|ged_prot_1|ged1_prot_1|g;s|ged_prot_2|ged1_prot_2|g"	
	;;
	"fc12" )
	rules="s|\"NumFQC\"|\"12\"|g;s|winTrans1TV1|winTrans2|g;s|winTrans2TV1|winTrans4|g;s|winTrans3TV1|winTrans1|g;s|winTrans4TV1|winTrans3|g;s|0xCEB0E1|0xCEB0E2|g;s|gatembn1|gate111|g;s|gatembn2|gate211|g;s|0xCEB0F1|0xCEB0F2|g;s|gaten1|gate112|g;s|gaten2|gate212|g;s|gaten3|gate111|g;s|gaten4|gate211|g;s|MPU1_CState_AS|FQC12_CState_AS|g;s|MPU2_CState_AS|FQC11_CState_AS|g;s|ged_plc_1|ged1_io_1|g;s|ged_plc_2|ged1_io_2|g;s|ged_prot_1|ged1_prot_1|g;s|ged_prot_2|ged1_prot_2|g"	
	;;
	"fc21" )
	rules="s|\"NumFQC\"|\"21\"|g;s|winTrans1TV2|winTrans1|g;s|winTrans2TV2|winTrans3|g;s|winTrans3TV2|winTrans2|g;s|winTrans4TV2|winTrans4|g;s|gatembn1|gate122|g;s|gatembn2|gate222|g;s|gaten1|gate121|g;s|gaten2|gate221|g;s|gaten3|gate122|g;s|gaten4|gate222|g;s|MPU1_CState_AS|FQC21_CState_AS|g;s|MPU2_CState_AS|FQC22_CState_AS|g;s|ged_plc_1|ged2_io_1|g;s|ged_plc_2|ged2_io_2|g;s|ged_prot_1|ged2_prot_1|g;s|ged_prot_2|ged2_prot_2|g;s|ConfirmMT=\"MT_ConfirmGED1_S\" TargetMT=\"MT_TargetGED1_S\"|ConfirmMT=\"MT_ConfirmGED2_S\" TargetMT=\"MT_TargetGED2_S\"|g"	
	;;
	"fc22" )
	rules="s|\"NumFQC\"|\"22\"|g;s|winTrans1TV2|winTrans2|g;s|winTrans2TV2|winTrans4|g;s|winTrans3TV2|winTrans1|g;s|winTrans4TV2|winTrans3|g;s|0xCEB0E1|0xCEB0E2|g;s|0xCEB0F1|0xCEB0F2|g;s|gatembn1|gate121|g;s|gatembn2|gate221|g;s|gaten1|gate122|g;s|gaten2|gate222|g;s|gaten3|gate121|g;s|gaten4|gate221|g;s|MPU1_CState_AS|FQC22_CState_AS|g;s|MPU2_CState_AS|FQC21_CState_AS|g;s|ged_plc_1|ged2_io_1|g;s|ged_plc_2|ged2_io_2|g;s|ged_prot_1|ged2_prot_1|g;s|ged_prot_2|ged2_prot_2|g;s|ConfirmMT=\"MT_ConfirmGED1_S\" TargetMT=\"MT_TargetGED1_S\"|ConfirmMT=\"MT_ConfirmGED2_S\" TargetMT=\"MT_TargetGED2_S\"|g"	
	;;
	"fc31" )
	rules="s|\"NumFQC\"|\"31\"|g;s|winTrans1TV3|winTrans1|g;s|winTrans2TV3|winTrans3|g;s|winTrans3TV3|winTrans2|g;s|winTrans4TV3|winTrans4|g;s|gatembn1|gate132|g;s|gatembn2|gate232|g;s|gaten1|gate131|g;s|gaten2|gate231|g;s|gaten3|gate132|g;s|gaten4|gate232|g;s|MPU1_CState_AS|FQC31_CState_AS|g;s|MPU2_CState_AS|FQC32_CState_AS|g;s|ged_plc_1|ged3_io_1|g;s|ged_plc_2|ged3_io_2|g;s|ged_prot_1|ged3_prot_1|g;s|ged_prot_2|ged3_prot_2|g;s|ConfirmMT=\"MT_ConfirmGED1_S\" TargetMT=\"MT_TargetGED1_S\"|ConfirmMT=\"MT_ConfirmGED3_S\" TargetMT=\"MT_TargetGED3_S\"|g"	
	;;
	"fc32" )
	rules="s|\"NumFQC\"|\"32\"|g;s|winTrans1TV3|winTrans2|g;s|winTrans2TV3|winTrans4|g;s|winTrans3TV3|winTrans1|g;s|winTrans4TV3|winTrans3|g;s|0xCEB0E1|0xCEB0E2|g;s|0xCEB0F1|0xCEB0F2|g;s|gatembn1|gate131|g;s|gatembn2|gate231|g;s|gaten1|gate132|g;s|gaten2|gate232|g;s|gaten3|gate131|g;s|gaten4|gate231|g;s|MPU1_CState_AS|FQC32_CState_AS|g;s|MPU2_CState_AS|FQC31_CState_AS|g;s|ged_plc_1|ged3_io_1|g;s|ged_plc_2|ged3_io_2|g;s|ged_prot_1|ged3_prot_1|g;s|ged_prot_2|ged3_prot_2|g;s|ConfirmMT=\"MT_ConfirmGED1_S\" TargetMT=\"MT_TargetGED1_S\"|ConfirmMT=\"MT_ConfirmGED3_S\" TargetMT=\"MT_TargetGED3_S\"|g"	
	;;
	esac

	subst "$rules" %buildroot%_sysconfdir/%name/configure.xml.$n
done

%post
ln -sf %_bindir/ctl-admin-%name.sh %_bindir/ctl-admin-mpu.sh
cat %_sysconfdir/openssh/authorized_keys2/22220.pub > %_sysconfdir/openssh/authorized_keys2/root
update_chrooted conf

# необходимо для скриптов ctl-XXXtohrml, и работы системы "сброса" журналов
ln -sf %_sysconfdir/%name/configure.xml %_sysconfdir/mpu3/configure.xml

ln -sf %_datadir/%name/alarmdb %_datadir/mpu3/alarmdb
mv -bf %_sysconfdir/%name/hosts %_sysconfdir/hosts
mv -bf %_sysconfdir/mpu3/userconf.d/mpu22220.conf %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template


%post gui
subst "s|SharedMemory1|SharedMemoryGUI|g" %_bindir/ctl-smviewer-%name.sh

%post gui-fc11
ln -sf %_bindir/ctl-gui-fc11.sh %_bindir/ctl-gui.sh 
ln -sf %_datadir/%name/*fc11-gui.ui %_datadir/%name/mpu22220-gui.ui
ln -sf %_sysconfdir/%name/configure.xml.fc11 %_sysconfdir/%name/configure.xml
ln -sf %_sysconfdir/%name/guiconfigure.xml.fc11 %_sysconfdir/%name/guiconfigure.xml
cat %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template | sed "s:gategate:gate111:g" > %_sysconfdir/mpu3/userconf.d/mpu22220.conf
rm -f %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template
echo "mpu22220-gui-fc11" > /etc/motd
subst s/HOSTNAME.*/HOSTNAME=gui-fc11/g /etc/sysconfig/network
cp /home/guest/.ssh/config.fc11 /home/guest/.ssh/config
subst "s|<LocalNode name=\"localhost\"/>|<LocalNode name=\"gui11\"/>|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r1_host:\"|arg=\"giop:tcp:mpu22220.gui11_1:\"|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r2_host:\"|arg=\"giop:tcp:mpu22220.gui11_2:\"|g" %_sysconfdir/mpu22220/configure.xml
subst "s/mtcall11_r1/mtcall11_r1 mtcall_r1/g" /etc/hosts
subst "s/mtcall11_r2/mtcall11_r2 mtcall_r2/g" /etc/hosts


%post gui-fc12
ln -sf %_bindir/ctl-gui-fc12.sh %_bindir/ctl-gui.sh  
ln -sf %_datadir/%name/*fc12-gui.ui %_datadir/%name/mpu22220-gui.ui
ln -sf %_sysconfdir/%name/configure.xml.fc12 %_sysconfdir/%name/configure.xml
ln -sf %_sysconfdir/%name/guiconfigure.xml.fc12 %_sysconfdir/%name/guiconfigure.xml
cat %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template | sed "s:gategate:gate112:g" > %_sysconfdir/mpu3/userconf.d/mpu22220.conf
rm -f %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template
echo "mpu22220-gui-fc12" > /etc/motd
subst s/HOSTNAME.*/HOSTNAME=gui-fc12/g /etc/sysconfig/network
cp /home/guest/.ssh/config.fc12 /home/guest/.ssh/config
subst "s|<LocalNode name=\"localhost\"/>|<LocalNode name=\"gui12\"/>|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r1_host:\"|arg=\"giop:tcp:mpu22220.gui12_1:\"|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r2_host:\"|arg=\"giop:tcp:mpu22220.gui12_2:\"|g" %_sysconfdir/mpu22220/configure.xml
subst "s/mtcall12_r1/mtcall12_r1 mtcall_r1/g" /etc/hosts
subst "s/mtcall12_r2/mtcall12_r2 mtcall_r2/g" /etc/hosts

%post gui-fc21
ln -sf %_bindir/ctl-gui-fc21.sh %_bindir/ctl-gui.sh  
ln -sf %_datadir/%name/*fc21-gui.ui %_datadir/%name/mpu22220-gui.ui
ln -sf %_sysconfdir/%name/configure.xml.fc21 %_sysconfdir/%name/configure.xml
ln -sf %_sysconfdir/%name/guiconfigure.xml.fc21 %_sysconfdir/%name/guiconfigure.xml
cat %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template | sed "s:gategate:gate121:g" > %_sysconfdir/mpu3/userconf.d/mpu22220.conf
rm -f %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template
echo "mpu22220-gui-fc21" > /etc/motd
subst s/HOSTNAME.*/HOSTNAME=gui-fc21/g /etc/sysconfig/network
cp /home/guest/.ssh/config.fc21 /home/guest/.ssh/config
subst "s|<LocalNode name=\"localhost\"/>|<LocalNode name=\"gui21\"/>|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r1_host:\"|arg=\"giop:tcp:mpu22220.gui21_1:\"|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r2_host:\"|arg=\"giop:tcp:mpu22220.gui21_2:\"|g" %_sysconfdir/mpu22220/configure.xml
subst "s/mtcall21_r1/mtcall21_r1 mtcall_r1/g" /etc/hosts
subst "s/mtcall21_r2/mtcall21_r2 mtcall_r2/g" /etc/hosts

%post gui-fc22
ln -sf %_bindir/ctl-gui-fc22.sh %_bindir/ctl-gui.sh  
ln -sf %_datadir/%name/*fc22-gui.ui %_datadir/%name/mpu22220-gui.ui
ln -sf %_sysconfdir/%name/configure.xml.fc22 %_sysconfdir/%name/configure.xml
ln -sf %_sysconfdir/%name/guiconfigure.xml.fc22 %_sysconfdir/%name/guiconfigure.xml
cat %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template | sed "s:gategate:gate122:g" > %_sysconfdir/mpu3/userconf.d/mpu22220.conf
rm -f %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template
echo "mpu22220-gui-fc22" > /etc/motd
subst s/HOSTNAME.*/HOSTNAME=gui-fc22/g /etc/sysconfig/network
cp /home/guest/.ssh/config.fc22 /home/guest/.ssh/config
subst "s|<LocalNode name=\"localhost\"/>|<LocalNode name=\"gui22\"/>|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r1_host:\"|arg=\"giop:tcp:mpu22220.gui22_1:\"|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r2_host:\"|arg=\"giop:tcp:mpu22220.gui22_2:\"|g" %_sysconfdir/mpu22220/configure.xml
subst "s/mtcall22_r1/mtcall22_r1 mtcall_r1/g" /etc/hosts
subst "s/mtcall22_r2/mtcall22_r2 mtcall_r2/g" /etc/hosts

%post gui-fc31
ln -sf %_bindir/ctl-gui-fc31.sh %_bindir/ctl-gui.sh  
ln -sf %_datadir/%name/*fc31-gui.ui %_datadir/%name/mpu22220-gui.ui
ln -sf %_sysconfdir/%name/configure.xml.fc31 %_sysconfdir/%name/configure.xml
ln -sf %_sysconfdir/%name/guiconfigure.xml.fc31 %_sysconfdir/%name/guiconfigure.xml
cat %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template | sed "s:gategate:gate131:g" > %_sysconfdir/mpu3/userconf.d/mpu22220.conf
rm -f %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template
echo "mpu22220-gui-fc31" > /etc/motd
subst s/HOSTNAME.*/HOSTNAME=gui-fc31/g /etc/sysconfig/network
cp /home/guest/.ssh/config.fc31 /home/guest/.ssh/config
subst "s|<LocalNode name=\"localhost\"/>|<LocalNode name=\"gui31\"/>|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r1_host:\"|arg=\"giop:tcp:mpu22220.gui31_1:\"|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r2_host:\"|arg=\"giop:tcp:mpu22220.gui31_2:\"|g" %_sysconfdir/mpu22220/configure.xml
subst "s/mtcall31_r1/mtcall31_r1 mtcall_r1/g" /etc/hosts
subst "s/mtcall31_r2/mtcall31_r2 mtcall_r2/g" /etc/hosts

%post gui-fc32
ln -sf %_bindir/ctl-gui-fc32.sh %_bindir/ctl-gui.sh  
ln -sf %_datadir/%name/*fc32-gui.ui %_datadir/%name/mpu22220-gui.ui
ln -sf %_sysconfdir/%name/configure.xml.fc32 %_sysconfdir/%name/configure.xml
ln -sf %_sysconfdir/%name/guiconfigure.xml.fc32 %_sysconfdir/%name/guiconfigure.xml
cat %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template | sed "s:gategate:gate132:g" > %_sysconfdir/mpu3/userconf.d/mpu22220.conf
rm -f %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template
echo "mpu22220-gui-fc32" > /etc/motd
subst s/HOSTNAME.*/HOSTNAME=gui-fc32/g /etc/sysconfig/network
cp /home/guest/.ssh/config.fc32 /home/guest/.ssh/config
subst "s|<LocalNode name=\"localhost\"/>|<LocalNode name=\"gui32\"/>|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r1_host:\"|arg=\"giop:tcp:mpu22220.gui32_1:\"|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r2_host:\"|arg=\"giop:tcp:mpu22220.gui32_2:\"|g" %_sysconfdir/mpu22220/configure.xml
subst "s/mtcall32_r1/mtcall32_r1 mtcall_r1/g" /etc/hosts
subst "s/mtcall32_r2/mtcall32_r2 mtcall_r2/g" /etc/hosts

%post gate-fc11
ln -sf %_bindir/ctl-smemory2-mpu22220-fc11.sh %_bindir/ctl-smemory2-mpu3.sh
echo "mpu22220-gate-fc11" > /etc/motd
subst s/HOSTNAME.*/HOSTNAME=gate-fc11/g /etc/sysconfig/network
ln -sf %_sysconfdir/%name/configure.xml.fc11 %_sysconfdir/%name/configure.xml
cat %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template | sed "s:gategate:gate111:g" > %_sysconfdir/mpu3/userconf.d/mpu22220.conf
rm -f %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template
subst "s|<LocalNode name=\"localhost\"/>|<LocalNode name=\"gate11\"/>|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r1_host:\"|arg=\"giop:tcp:mpu22220.gate11_1:\"|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r2_host:\"|arg=\"giop:tcp:mpu22220.gate11_2:\"|g" %_sysconfdir/mpu22220/configure.xml

%post gate-fc12
ln -sf %_bindir/ctl-smemory2-mpu22220-fc12.sh %_bindir/ctl-smemory2-mpu3.sh
ln -sf %_sysconfdir/%name/configure.xml.fc12 %_sysconfdir/%name/configure.xml
echo "mpu22220-gate-fc12" > /etc/motd
subst s/HOSTNAME.*/HOSTNAME=gate-fc12/g /etc/sysconfig/network
cat %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template | sed "s:gategate:gate112:g" > %_sysconfdir/mpu3/userconf.d/mpu22220.conf
rm -f %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template
subst "s|<LocalNode name=\"localhost\"/>|<LocalNode name=\"gate12\"/>|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r1_host:\"|arg=\"giop:tcp:mpu22220.gate12_1:\"|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r2_host:\"|arg=\"giop:tcp:mpu22220.gate12_2:\"|g" %_sysconfdir/mpu22220/configure.xml

%post gate-fc21
ln -sf %_bindir/ctl-smemory2-mpu22220-fc21.sh %_bindir/ctl-smemory2-mpu3.sh
echo "mpu22220-gate-fc21" > /etc/motd
ln -sf %_sysconfdir/%name/configure.xml.fc21 %_sysconfdir/%name/configure.xml
subst s/HOSTNAME.*/HOSTNAME=gate-fc21/g /etc/sysconfig/network
cat %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template | sed "s:gategate:gate121:g" > %_sysconfdir/mpu3/userconf.d/mpu22220.conf
rm -f %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template
subst "s|<LocalNode name=\"localhost\"/>|<LocalNode name=\"gate21\"/>|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r1_host:\"|arg=\"giop:tcp:mpu22220.gate21_1:\"|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r2_host:\"|arg=\"giop:tcp:mpu22220.gate21_2:\"|g" %_sysconfdir/mpu22220/configure.xml

%post gate-fc22
ln -sf %_bindir/ctl-smemory2-mpu22220-fc22.sh %_bindir/ctl-smemory2-mpu3.sh
ln -sf %_sysconfdir/%name/configure.xml.fc22 %_sysconfdir/%name/configure.xml
echo "mpu22220-gate-fc22" > /etc/motd
subst s/HOSTNAME.*/HOSTNAME=gate-fc22/g /etc/sysconfig/network
cat %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template | sed "s:gategate:gate122:g" > %_sysconfdir/mpu3/userconf.d/mpu22220.conf
rm -f %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template
subst "s|<LocalNode name=\"localhost\"/>|<LocalNode name=\"gate22\"/>|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r1_host:\"|arg=\"giop:tcp:mpu22220.gate22_1:\"|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r2_host:\"|arg=\"giop:tcp:mpu22220.gate22_2:\"|g" %_sysconfdir/mpu22220/configure.xml

%post gate-fc31
ln -sf %_bindir/ctl-smemory2-mpu22220-fc31.sh %_bindir/ctl-smemory2-mpu3.sh
echo "mpu22220-gate-fc31" > /etc/motd
subst s/HOSTNAME.*/HOSTNAME=gate-fc31/g /etc/sysconfig/network
ln -sf %_sysconfdir/%name/configure.xml.fc31 %_sysconfdir/%name/configure.xml
cat %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template | sed "s:gategate:gate131:g" > %_sysconfdir/mpu3/userconf.d/mpu22220.conf
rm -f %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template
subst "s|<LocalNode name=\"localhost\"/>|<LocalNode name=\"gate31\"/>|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r1_host:\"|arg=\"giop:tcp:mpu22220.gate31_1:\"|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r2_host:\"|arg=\"giop:tcp:mpu22220.gate31_2:\"|g" %_sysconfdir/mpu22220/configure.xml

%post gate-fc32
ln -sf %_bindir/ctl-smemory2-mpu22220-fc32.sh %_bindir/ctl-smemory2-mpu3.sh
ln -sf %_sysconfdir/%name/configure.xml.fc32 %_sysconfdir/%name/configure.xml
echo "mpu22220-gate-fc32" > /etc/motd
subst s/HOSTNAME.*/HOSTNAME=gate-fc32/g /etc/sysconfig/network
cat %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template | sed "s:gategate:gate132:g" > %_sysconfdir/mpu3/userconf.d/mpu22220.conf
rm -f %_sysconfdir/mpu3/userconf.d/mpu22220.conf.template
subst "s|<LocalNode name=\"localhost\"/>|<LocalNode name=\"gate32\"/>|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r1_host:\"|arg=\"giop:tcp:mpu22220.gate32_1:\"|g" %_sysconfdir/mpu22220/configure.xml
subst "s|arg=\"giop:tcp:r2_host:\"|arg=\"giop:tcp:mpu22220.gate32_2:\"|g" %_sysconfdir/mpu22220/configure.xml

%postun

%files
%config %_sysconfdir/%name/guiconfigure.xml
%_sysconfdir/%name/hosts
%_sysconfdir/cron.d/time
%_sysconfdir/apt/sources.list.d/mpu22220.list
%config %_sysconfdir/mpu3/userconf.d/mpu22220.conf
%_sysconfdir/openssh/authorized_keys2/22220.pub
%dir %_datadir/%name/
%_datadir/%name/VERSION.txt
%_datadir/mpu3/VERSION.txt
%_datadir/%name/alarmdb/alarmdb.xml
%_datadir/%name/theme/*
%_datadir/%name/images/*
%_datadir/%name/gtkrc
%exclude %_bindir/smemo*
%_bindir/ctl-admin-mpu22220.sh
%_bindir/ctl-functions-mpu22220.sh
%_bindir/ctl-install.sh
%_bindir/ctl-smonit-mpu22220.sh
%_bindir/ctl-smviewer-mpu22220.sh
#%_bindir/ctl-update.sh


%attr(0777,root,root) %dir /var/local/log/%name
%attr(0777,root,root) %dir /var/local/run/%name
%attr(0777,root,root) %dir /var/local/lock/%name
%attr(0777,root,root) %dir /var/local/cache/%name
%attr(0777,root,root) %dir /var/local/omniORB/
%attr(0777,guest,root) %dir /var/local/%name/db
%attr(0777,guest,root) %dir /var/local/%name/backup

%files -n %name-gate
%_bindir/smemory2-*

%files -n %name-gate-fc11
%_bindir/ctl-smemory2-mpu22220-fc11.sh
%config %_sysconfdir/%name/configure.xml.fc11

%files -n %name-gate-fc12
%_bindir/ctl-smemory2-mpu22220-fc12.sh
%config %_sysconfdir/%name/configure.xml.fc12

%files -n %name-gate-fc21
%_bindir/ctl-smemory2-mpu22220-fc21.sh
%config %_sysconfdir/%name/configure.xml.fc21

%files -n %name-gate-fc22
%_bindir/ctl-smemory2-mpu22220-fc22.sh
%config %_sysconfdir/%name/configure.xml.fc22

%files -n %name-gate-fc31
%_bindir/ctl-smemory2-mpu22220-fc31.sh
%config %_sysconfdir/%name/configure.xml.fc31

%files -n %name-gate-fc32
%_bindir/ctl-smemory2-mpu22220-fc32.sh
%config %_sysconfdir/%name/configure.xml.fc32

%files -n %name-gui
%_bindir/mpu22220-gui
/home/guest/.ssh/config.fc11
/home/guest/.ssh/config.fc12
/home/guest/.ssh/config.fc21
/home/guest/.ssh/config.fc22
/home/guest/.ssh/config.fc31
/home/guest/.ssh/config.fc32
/home/guest/.ssh/id_dsa

%files -n %name-gui-fc11
%_datadir/%name/*fc11-gui.ui
%_bindir/ctl-gui-fc11.sh
%config %_sysconfdir/%name/configure.xml.fc11
%config %_sysconfdir/%name/guiconfigure.xml.fc11

%files -n %name-gui-fc12
%_datadir/%name/*fc12-gui.ui
%_bindir/ctl-gui-fc12.sh
%config %_sysconfdir/%name/configure.xml.fc12
%config %_sysconfdir/%name/guiconfigure.xml.fc12

%files -n %name-gui-fc21
%_datadir/%name/*fc21-gui.ui
%_bindir/ctl-gui-fc21.sh
%config %_sysconfdir/%name/configure.xml.fc21
%config %_sysconfdir/%name/guiconfigure.xml.fc21

%files -n %name-gui-fc22
%_datadir/%name/*fc22-gui.ui
%_bindir/ctl-gui-fc22.sh
%config %_sysconfdir/%name/configure.xml.fc22
%config %_sysconfdir/%name/guiconfigure.xml.fc22

%files -n %name-gui-fc31
%_datadir/%name/*fc31-gui.ui
%_bindir/ctl-gui-fc31.sh
%config %_sysconfdir/%name/configure.xml.fc31
%config %_sysconfdir/%name/guiconfigure.xml.fc31

%files -n %name-gui-fc32
%_datadir/%name/*fc32-gui.ui
%_bindir/ctl-gui-fc32.sh
%config %_sysconfdir/%name/configure.xml.fc32
%config %_sysconfdir/%name/guiconfigure.xml.fc32

%if_enabled doc
%files -n %name-doc
%_docdir/%name-%version/
%endif

%changelog
* Fri Jul 24 2020 Ura Nechaev <nechaev@server> 0.3-set42.43
- (autobuild): commit 3b1ab9574d6a99be83f1c774bedeff764c47149e

* Tue Jun 16 2020 Ura Nechaev <nechaev@server> 0.3-set42.40
- (autobuild): commit a80cec184f6fb58e6e23ed7ac3485313c9efaf56

* Mon May 25 2020 Ura Nechaev <nechaev@server> 0.3-set42.11
- (autobuild): commit 4d73d7592f60c9b62bf34f271aceee0baca6582f

* Tue Mar 24 2020 Ura Nechaev <nechaev@server> 0.3-set41.73
- (autobuild): commit ffdd8938e3036f207c05145103ef93411723c05b

* Fri Feb 14 2020 Ura Nechaev <nechaev@server> 0.3-set41.66
- (autobuild): commit fedb8e17bf6b5aea5cfa7068f14d6c1dda476df2

* Fri Feb 14 2020 Ura Nechaev <nechaev@server> 0.3-set41.65
- (autobuild): commit 7b96c88b266478c5ac1b2ea1b9322bdd9c7fabed

* Fri Feb 14 2020 Ura Nechaev <nechaev@server> 0.3-set41.62
- (autobuild): commit 8cc7ebf68b4e8ef52d86d9fac434ab6b95b4c2be

* Fri Feb 14 2020 Ura Nechaev <nechaev@server> 0.3-set41.61
- (autobuild): commit 31273a2ea29fd7f4ef32d2479da8e6d5c2db604e

* Wed Feb 05 2020 Ura Nechaev <nechaev@server> 0.3-set41.51
- (autobuild): commit e844c60d1046e1e9dc97b60faa10c1ada9fad8ed

* Mon Nov 11 2019 un 0.3-set28
- new build

* Mon Nov 11 2019 un 0.3-set28
- new build

* Mon Nov 11 2019 un 0.3-set28
- new build

* Tue May 22 2018 Ura Nechaev <nechaev@server> 0.3-set20
- new build

* Tue May 22 2018 Ura Nechaev <nechaev@server> 0.3-set19
- new build

* Tue May 15 2018 Ura Nechaev <nechaev@server> 0.3-set18
- new build

* Thu May 10 2018 Ura Nechaev <nechaev@server> 0.3-set17
- new build

* Thu May 10 2018 Ura Nechaev <nechaev@server> 0.3-set16
- new build

* Tue May 08 2018 Ura Nechaev <nechaev@server> 0.3-set15
- new build

* Mon May 07 2018 Ura Nechaev <nechaev@server> 0.3-set14
- new build

* Mon May 07 2018 Ura Nechaev <nechaev@server> 0.3-set13
- new build

* Mon May 07 2018 Ura Nechaev <nechaev@server> 0.3-set12
- new build

* Sat Apr 28 2018 Ura Nechaev <nechaev@server> 0.3-set11
- new build

* Sat Apr 28 2018 Ura Nechaev <nechaev@server> 0.3-set10
- new build

* Fri Apr 27 2018 Ura Nechaev <nechaev@server> 0.3-set9
- new build

* Fri Apr 27 2018 Ura Nechaev <nechaev@server> 0.3-set8
- new build

* Thu Apr 26 2018 Ura Nechaev <nechaev@server> 0.3-set7
- new build

* Thu Apr 26 2018 Ura Nechaev <nechaev@server> 0.3-set6
- new build

* Thu Apr 26 2018 Ura Nechaev <nechaev@server> 0.3-set5
- new build

* Thu Apr 26 2018 Ura Nechaev <nechaev@server> 0.3-set4
- new build

* Thu Apr 26 2018 Ura Nechaev <nechaev@server> 0.3-set3
- new build

* Thu Apr 26 2018 Ura Nechaev <nechaev@server> 0.3-set2
- new build

* Wed Nov 15 2017 Ura Nechaev <nechaev@server> 0.3-set1
- new build

* Thu Sep 15 2016 Pavel Vainerman <pv@altlinux.ru> 0.1-set1
- update reqiures
- WarningsWindow: modify selected color

* Wed Jul 27 2016 Ura Nechaev <nechaev@server> 0.1-set0.185
- new build

* Fri Jul 01 2016 Ura Nechaev <nechaev@server> 0.1-set0.170
- new build

* Tue Jun 21 2016 Ura Nechaev <nechaev@server> 0.1-set0.131
- new build

* Wed Mar 23 2016 Ura Nechaev <nechaev@server> 0.1-set0.87
- new build

* Wed Mar 23 2016 Ura Nechaev <nechaev@server> 0.1-set0.86
- new build

* Wed Mar 23 2016 Ura Nechaev <nechaev@server> 0.1-set0.81
- new build

* Fri Feb 12 2016 Ura Nechaev <nechaev@server> 0.1-set0.38
- new build

* Wed Feb 10 2016 Ura Nechaev <nechaev@server> 0.1-set0.35
- new build

* Tue Feb 09 2016 Ura Nechaev <nechaev@server> 0.1-set0.33
- new build

* Mon Jan 25 2016 Ura Nechaev <nechaev@server> 0.1-set0.26
- new build

* Tue Dec 22 2015 Ura Nechaev <nechaev@server> 0.1-set0.23
- new build

* Tue Dec 22 2015 Ura Nechaev <nechaev@server> 0.1-set0.21
- new build

* Tue Dec 22 2015 Ura Nechaev <nechaev@server> 0.1-set0.19
- new build

* Thu Dec 17 2015 Ura Nechaev <nechaev@server> 0.1-set0.5
- new build

* Sun Dec 13 2015 Ura Nechaev <nechaev@server> 0.1-set0.1
- new build

