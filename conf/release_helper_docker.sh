#!/bin/sh
# Вспомогательный скрипт для подготовки и сборки rpm-пакета с системой
ETERBUILDVERSION=163
. /usr/share/eterbuild/eterbuild
load_mod spec

REL=setri
MAILDOMAIN=server

[ -z "$TOPDIR" ] && TOPDIR=/var/ftp/pub/Ourside

PKGNAME=mpu22220
SPECNAME=mpu22220.spec

#if [ -z "$PLATFORM" ]; then
PLATFORM=i586
#[[ `uname -m` == "x86_64" ]] && PLATFORM=x86_64
#fi

PROJECT=$1
test -n "$PROJECT" || PROJECT=$PKGNAME

[ -z "$GEN" ] && GEN=/var/ftp/pub/Ourside/$PLATFORM/genb.sh
[ -a "$GEN" ] || GEN="genbasedir --create --progress --topdir=$TOPDIR $PLATFORM $PROJECT"

[ -z "$FTPDIR" ] && FTPDIR=$TOPDIR/$PLATFORM/RPMS.$PROJECT
[ -z "$BACKUPDIR" ] && BACKUPDIR=$FTPDIR/backup

fatal()
{
	echo "Error: $@"
	exit 1
}

function cp2ftp()
{
	RPMBINDIR=$RPMDIR/RPMS
	test -d $RPMBINDIR/$PLATFORM && RPMBINDIR=$RPMBINDIR/$PLATFORM
	mkdir -p $BACKUPDIR
	mkdir -p $FTPDIR
	mv -f $FTPDIR/*$PKGNAME* $BACKUPDIR/
	mv -f $RPMBINDIR/*$PKGNAME* $FTPDIR/
	chmod 'a+rw' $FTPDIR/*$PKGNAME*
	$GEN
}



nice gear -v --commit --rpmbuild -- rpmbuild '--define=_topdir /home/user/RPM' --quiet --quiet '--define=_unpackaged_files_terminate_build 0' --quiet -bb --target i586
cp2ftp

