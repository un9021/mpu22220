<?xml version='1.0' encoding="utf-8" ?>
<xsl:stylesheet  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'
	             xmlns:date="http://exslt.org/dates-and-times">

<xsl:output method="html" indent="yes" encoding="cp1251"/>
<xsl:template match="Dump">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=cp1251"/>
<title>Дамп параметров</title>
</head>
<body>
<h1>Дамп параметров</h1>
<xsl:apply-templates select="Item">
	<xsl:with-param name="size" select="@size"/>
</xsl:apply-templates>
</body>
</html>
</xsl:template>

<xsl:template match="Item">
<xsl:param name="size"/>
<h3>
<xsl:if test="@hour &lt; 10">0</xsl:if><xsl:value-of select="@hour"/>:<xsl:if test="@min &lt; 10">0</xsl:if><xsl:value-of select="@min"/>:<xsl:if test="@sec &lt; 10">0</xsl:if><xsl:value-of select="@sec"/>&#160;<xsl:if test="@day &lt; 10">0</xsl:if><xsl:value-of select="@day"/>/<xsl:if test="@mon &lt; 10">0</xsl:if><xsl:value-of select="@mon"/>/<xsl:if test="@year &lt; 10">0</xsl:if><xsl:value-of select="@year"/>
</h3>
<table border="1">
	<tr>
		<th style="width:300px;">ID датчика</th>
		<th style="width:300px;">Значение</th>
	</tr>
	<xsl:apply-templates select="Record"/>
</table>
<hr/>
</xsl:template>

<xsl:template match="Record">
<tr>
	<th style="width:300px;">
		<xsl:value-of select="@id"/>
	</th>
	<th style="width:300px;">
		<xsl:value-of select="@value"/>
	</th>
</tr>
</xsl:template>

</xsl:stylesheet>
